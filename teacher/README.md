# TribeCoding
Teach by example. Blog as you code.
## Structure
Tribe coding is build from as multilayer application
- Client
- Server
- FileManager


## Production build
Run following command

```npm run build```

build output will be available in **build** directory

!Output files will be used by server container build to serve files from the server!

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory} from 'react-router'
import { UploadRoute } from './UploadRoute';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core';
import { green, grey } from '@material-ui/core/colors';
import CookieConsent from 'react-cookie-consent';
import ReactGA from 'react-ga';

ReactGA.initialize('UA-153377497-1');

const theme = createMuiTheme({
  palette: {
    primary: green,
    secondary: grey
  }
});

const appElement = document.getElementById('app');
ReactDOM.render((
  <React.Fragment>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Router history={browserHistory}>
          <Route path="/" component={UploadRoute}/>
        </Router>
        <CookieConsent style={{ padding: "15px" }}>
          This website uses cookies to enhance the user experience. For more details see our <a href="cookies_policy.html">Cookies Policy</a>
        </CookieConsent>
      </ThemeProvider>
    </React.Fragment>
)
, appElement);

export default class Api {
  static server = process.env.NODE_ENV === 'development' ? 'http://localhost:3001':'';

  static async fetch(url: string, params?: RequestInit, asText = false,  error = 'Failed to communicate with tribecoding API!') {
    try {
      const response =  params ? await fetch(`${Api.server}${url}`, params) : await fetch(`${Api.server}${url}`);
      if (response) {
        const  data = asText ? await response.text() : await response.json();
        if (data) {
          return data;
        }
      }
    } catch {
      console.error(error);
    }
  }
}
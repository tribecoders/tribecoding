import Api from './Api';
import Lesson from './Lesson';

export default class LessonApi extends Api {
  static async getLesson(lessonId: string): Promise<Lesson | undefined> {
    try {
    const data = await this.fetch(`/api/lesson/${lessonId}`);
    const lesson = Lesson.fromApiData(data);
    return lesson;
    } catch {
      console.error('Failed to load lesson!');
    }
  }

  static async postLesson(repository: string): Promise<string | undefined> {
    try {
      const params = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          'repository': repository
        })
      };
      const data = await this.fetch(`/api/lesson`, params);
      return data.lessonId;
    } catch {
      console.error('Fail to add new lesson!');
    }
  }
} 
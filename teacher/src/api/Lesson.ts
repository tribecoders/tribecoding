export default class Lesson {
  id: string;
  name: string;
  slug: string;
  repository: string;

  constructor(id: string, name: string, slug: string, repository: string) {
    this.id = id;
    this.name = name;
    this.slug = slug;
    this.repository = repository;
  }

  static fromApiData(data: any) {
    return new Lesson(data.id, data.name, data.slug, data.repository);
  }
}
import React from 'react';
import { Container, Button, TextField, Grid, LinearProgress } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import styles from "./Upload.module.scss";
import Link from 'react-router/lib/Link';
import Error, { ErrorContent } from "../Error";
import ReactGA from 'react-ga';
import { LessonApi } from '../api';

export interface UploadState {
  lessonRepository: string;
  uploadInProgress: boolean;
  uploadSucess: boolean;
  error?: ErrorContent;
}

export default class Upload extends React.Component<{}, UploadState> {
  state = {
    lessonRepository: "",
    uploadInProgress: false,
    uploadSucess: false,
    error: undefined,
  }

  componentDidMount() {
    ReactGA.pageview('/Upload');
  }

  sendForm() {
    ReactGA.event({
      category: 'Upload',
      action: 'Upload'
    });
    if (this.state.lessonRepository) {
      this.setState({
        uploadInProgress: true,
        error: undefined
      });
      LessonApi.postLesson(this.state.lessonRepository).then(lessonId => {
         if (lessonId) {
          this.setState({
            lessonRepository: "",
            uploadInProgress: false,
            uploadSucess: true,
          });
          ReactGA.event({
            category: 'Upload',
            action: 'Success'
          });
          LessonApi.getLesson(lessonId).then(lesson => {
            if (lesson) {
              window.location.replace(`http://tribecoding.com/learn/${lesson.slug}`);
            } else {
              this.uploadError('Lesson load failed!');
            }
          }).catch( ()=> {
            this.uploadError('Cannot get a lesson!');
          }); 
         } else {
          this.uploadError('Your repository was not parssed correctly.', 'Parsing error!');
        }
      }).catch(() => {
        this.uploadError('We had a problem communicating with the server.', 'Communication error!');
      });
    }
  }

  uploadError(message: string, title?: string){
    ReactGA.event({
      category: 'Upload',
      action: 'Error'
    });
    this.setState({
      uploadInProgress: false,
      error: new ErrorContent(message),
    });
  }

  updateInputValue(evt: any) {
    this.setState({
      lessonRepository: evt.target.value
    });
  }

  render() {
    return (
      <div>
        <Container fixed>
          <Grid container spacing={3} className={styles.header}>
            <Grid item xs={6}>
              <div className={styles.logo}>
                <Link to="/">
                  <div className={styles.title}>
                    <img src="/images/logo.png" srcSet="/images/logo@2x.png 2x, /images/logo.png 1x" alt="back"/>
                    <div>TribeCoding Teacher</div>
                    </div>
                </Link>
              </div>
            </Grid>
            <Grid item xs={6} className={styles.headerRight}>
              <a href="http://tribecoding.com">checkout other lessons</a>
            </Grid>
          </Grid>
          {this.state.uploadSucess ? (
            <div className={styles.thankYou}>Thank you for uploading!</div>
          ) : (
            <div>
              {this.state.uploadInProgress ? (
                <LinearProgress className={styles.progress}/>
              ) : (
                <div className={styles.inputContainer}>
                  <TextField
                  id="outlined-input"
                  label="Public repository URL"
                  type="text"
                  name="email"
                  autoComplete="email"
                  margin="normal"
                  variant="outlined"
                  value={this.state.lessonRepository} 
                  error={this.state.error}
                  onChange={evt => this.updateInputValue(evt)}
                />
                  <Error error={this.state.error} className={styles.error}></Error>
                  <Button 
                    onClick={this.sendForm.bind(this)} 
                    color="primary"
                    variant="contained" 
                    size="large" 
                    startIcon={<CloudUploadIcon />}
                    className ={styles.button}
                  >
                        Post a lesson
                  </Button>
                </div>
              )}

            <div className={styles.howTo}>

              <h1>Posting at tribecoding.com</h1>
              <video className={styles.uploadVideo} src="/images/tribecoding.mp4" autoPlay loop muted controls></video>
              <br />Download pdf presentation <a href="/images/tribecoding.pdf" download>here</a>
              
              <h2>Demo</h2>
              See our <a href="http://tribecoding.com">lessons</a> created using tribecoding technology.

              <h2>How to</h2>
              Posting a technical lesson has never been so easy. You can do it, while writing your code. 

                <ul>
                  <li>Select commits, that you think represent interesting state of the application development</li>
                  <li>Create a <i>*.md file</i> (blog page)describing what you did (what is interesting).</li>
                  <li>At the end, just create <i>tribecoding.json</i> file in the root directory (like other configuration files).</li>
                  <li>Publish publically your repository. You can host repository yourself or at any public vendors such as GitHub or GitLab.</li>
                  <li>Submit a link to your repository in the input above. We will process it an post your lesson online.</li>
                  <li>If you like the result <a href="mailto:contact@tribecoding.com">contact us</a> to make yout lesson available at <a href="http://tribecoding.com">http://tribecoding.com</a>.</li>
                </ul>
              <h2>tribecoding.json format</h2>
                Tribecoding file is similar to other configuration files. Bellow you can find JSON structure definition: 
                <pre>{`
{
  "name": string,
  "tags": [string],
  "source": string,
  "image" : string,
  "chapters": [{
    "chapter": string,
    "commit": string,
    "file": string,
  }]
}
                `}</pre>

                <ul>
                  <li>name - Unique name of your technical lesson</li>
                  <li>tags - List of used technologies. Eg. React, Angular, Pixi, TypeScript, ...</li>
                  <li>source - Relative address to your project source files</li>
                  <li>image - url to publically stored image representing your blog. To load faster we just need it to be 50x50.</li>
                  <li>chapters - list of your technical blog pages
                    <ul>
                      <li>chapter - chapter title</li>
                      <li>commit - commit hash that represent the lesson chapter</li>
                      <li>file - <i>*.md file</i> name containing blog page. You do not need to add ".md" extension.</li>
                    </ul>
                  </li>
                </ul>

                

                <h2>Example</h2>
                  You can find example of tribeconding.json file <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/tribecoders/petwarsclient/blob/master/tribecoding.json">here</a>. 
                  <pre>{`
{
  {
    "name": "Learn React with Pet Wars",
    "image": "https://assets.gitlab-static.net/uploads/-/system/project/avatar/15752671/dog.png?width=48",
    "tags": ["React","TypeScript"],
    "source": "./src",
    "chapters": [{
      "chapter": "React and games",
      "commit": "afe46ddcecaac4a46e89fe77e73afd8ec2615241",
      "file": "chapter00"
    },{
      "chapter": "Components",
      "commit": "afe46ddcecaac4a46e89fe77e73afd8ec2615241",
      "file": "chapter01"
    },{
      "chapter": "Applying CSS",
      "commit": "83ea3b0171c6b17b455e242e616ba47e8b185f5c",
      "file": "chapter02"
    }]
  }
}
                `}</pre>


                <h2>Updates</h2>
                Both <i>tribecoding.json</i> and <i>*.md files</i> are fetched from master branch latest commit. This allow further updates to those files. 
                Just re-submit your repository again. Tribecoding make the latest version of your repository available as a lesson.
              </div>
            </div>
          )}   
        </Container> 
        <Grid container spacing={3} className={styles.footer}>
          <Grid item md={6} className={styles.footerText}>
            Contact us <a href="mailto:contact@tribecoding.com">contact@tribecoding.com</a><br />
            Developed by <a href="tribecoders.com">tribecoders.com</a> 
          </Grid>
          <Grid item md={6} className={styles.footerText}>
            <a href="terms_and_conditions.html">Terms and conditions</a><br />
            <a href="cookies_policy.html">Cookies policy</a><br />
            <a href="privacy_policy.html">Privacy policy</a><br />
          </Grid>
        </Grid>
      </div>
    )
  }
} 
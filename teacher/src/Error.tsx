import React from 'react';
import styles from "./Error.module.scss";
export class ErrorContent {
  title?: string
  message?: string

  constructor(message: string, title?: string) {
    this.message = message
    this.title = title
  }
}

export interface ErrorProps {
  error?: ErrorContent
  message?: string
  className?: string
}

export default class Error extends React.Component<ErrorProps, {}> {
  render() {
    return (
      <div>
        {this.props.error && 
          <div className={this.props.className}>
            <div className={styles.error}>
              <div>{this.props.error.title}</div>
              <div>{this.props.error.message}</div>
              <div>Please contact <a href="mailto:support@tribecoding.com">support@tribecoding.com</a> if you need assistance.</div>
            </div>
          </div>
        }
        {this.props.message && 
          <div className={this.props.className}>
            <div className={styles.error}>
              <div>{this.props.message}</div>
              <div>Please contact <a href="mailto:support@tribecoding.com">support@tribecoding.com</a> if you need assistance.</div>
            </div>
          </div>
        }
      </div>
    )
  }

}
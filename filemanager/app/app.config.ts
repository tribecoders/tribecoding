
import ProjectController from './ProjectController';
import LessonController from './LessonController';
import { Container } from "inversify";
import LessonStorage from './LessonStorage';
import ChapterStorage from './ChapterStorage';
import ChapterController from './ChapterController';
import ProjectStorage from './ProjectStorage';

export default function configureContainer() {
  var container = new Container();
  container.bind(LessonController).toSelf();
  container.bind(ChapterController).toSelf();
  container.bind(ProjectController).toSelf();
  container.bind(LessonStorage).toSelf();
  container.bind(ChapterStorage).toSelf();
  container.bind(ProjectStorage).toSelf();
  return container;
}
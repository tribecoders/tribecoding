export interface TribeCoding {
  chapters: TribeCodingChapter[];
  source: string;
}

export interface TribeCodingChapter {
  chapter: string;
  branch: string;
  file: string;
}

import { injectable, inject } from 'inversify';
import { Request, Response } from 'express-serve-static-core';
import ProjectStorage from './ProjectStorage';

@injectable()
export default class ProjectController {
  @inject(ProjectStorage) projectStorage!: ProjectStorage;

  async getProjectFiles(req: Request, res: Response) {
    const revision = req.params.revision;
    const chapterId = req.params.chapterId;
    if (!revision || !chapterId) {
      res.send(false);
      return;
    }
    console.log(`GET /lessonRevision/${revision}/chapter/${chapterId}/file`);
    try {
      const files = await this.projectStorage.getProjectFiles('asan', revision, chapterId);
      res.send(files);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  async getProjectFile(req: Request, res: Response) {
    const revision = req.params.revision;
    const chapterId = req.params.chapterId;
    const fileName = req.params.fileName;
    if (!revision || !chapterId || !fileName) {
      res.send(false);
      return;
    }
    console.log(`GET /lessonRevision/${revision}/chapter/${chapterId}/file/${fileName}`);
    try {
      const file = await this.projectStorage.getProjectFile('asan', revision, chapterId, fileName);
      res.send(file);

    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }
  
  async putProjectFile(req: Request, res: Response) {
    const revision = req.params.revision;
    const chapterId = req.params.chapterId;
    const fileName = req.params.fileName;
    const content = req.body;
    if (!revision || !chapterId || !fileName) {
      res.send(false);
      return;
    }
    console.log(`PUT /lessonRevision/${revision}/chapter/${chapterId}/file/${fileName}`)
    try {
      await this.projectStorage.setProjectFile('asan', revision, chapterId, fileName, content);
      res.send(true);

    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  async putProjectFiles(req: Request, res: Response) {
    const revision = req.params.revision;
    const chapterId = req.params.chapterId;
    const content = req.body;
    if (!revision || !chapterId) {
      res.send(false);
      return;
    }
    console.log(`PUT /lessonRevision/${revision}/chapter/${chapterId}/file`)
    try {
      await this.projectStorage.restoreProjectFiles('asan', revision, chapterId);
      res.send(true);

    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  async publish(req: Request, res: Response) {

  }
}
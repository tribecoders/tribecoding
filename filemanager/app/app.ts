// lib/app.ts
import "reflect-metadata";
import express from 'express';
import bodyParser = require('body-parser');
import configureContainer from './app.config';
import LessonController from './LessonController';
import ChapterController from './ChapterController';
import ProjectController from './ProjectController';
import dotenv from 'dotenv';

dotenv.config();

// Create a new express application instance
const app: express.Application = express();

app.use(bodyParser.json());
app.use(bodyParser.text());
//app.use(bodyParser.urlencoded({ extended: true }));

const container = configureContainer();
const lessonController = container.get(LessonController);
const chapterController = container.get(ChapterController);
const projectController = container.get(ProjectController);

//Publis globally user file
app.use('/public', express.static(__dirname + '/public'));

// Lesson
app.route('/lessonRevision/:revision').post(lessonController.postLesson.bind(lessonController));
app.route('/lessonRevision/:revision').get(lessonController.getLesson.bind(lessonController));

// Lesson chapter
app.route('/lessonRevision/:revision/chapter/:chapterId').post(chapterController.postChapter.bind(chapterController));
app.route('/lessonRevision/:revision/chapter/:file').get(chapterController.getChapter.bind(chapterController));

// Lesson files - user project 
app.route('/lessonRevision/:revision/chapter/:chapterId/file').get(projectController.getProjectFiles.bind(projectController));
app.route('/lessonRevision/:revision/chapter/:chapterId/file').put(projectController.putProjectFiles.bind(projectController));
app.route('/lessonRevision/:revision/chapter/:chapterId/file/:fileName').get(projectController.getProjectFile.bind(projectController));
app.route('/lessonRevision/:revision/chapter/:chapterId/file/:fileName').put(projectController.putProjectFile.bind(projectController));

//Start app
app.listen(3002, function () {
	console.log('Example app listening on port 3002!');
});
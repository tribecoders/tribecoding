import { Request, Response} from 'express-serve-static-core';
import { injectable, inject } from "inversify";
import LessonStorage from './LessonStorage';

@injectable()
export default class LessonController {
  @inject(LessonStorage) private lessonStorage!: LessonStorage;

  public async postLesson(req: Request, res: Response) {
    const revision = req.params.revision
    const lessonGitUrl = req.body.repository;
    if (!revision || !lessonGitUrl) {
      res.send(false);
      return;
    }
    console.log(`POST /lessonRevision/${revision} -> ${lessonGitUrl}`);
    try {
      
      await this.lessonStorage.createLesson(revision, lessonGitUrl);
      res.send(true);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async getLesson(req: Request, res: Response) {
    const revision = req.params.revision
    if (!revision) {
      res.send(false);
      return;
    }
    console.log(`GET /lessonRevision/${revision}`);
    try {
      const tribecoding = await this.lessonStorage.getLessonTribeCoding(revision);
      res.send(tribecoding);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }
}
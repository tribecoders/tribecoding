export default class Chapter {
  id: string| undefined;
  lessonId: string;
  name: string;
  file: string;
  chapterNo: number;

  constructor(id: string | undefined, lessonId:string, name:string, file: string, chapterNo:number) {
    if (id) {
      this.id = id;
    }
    this.lessonId = lessonId;
    this.name = name;
    this.chapterNo = chapterNo;
    this.file = file;
  }
}
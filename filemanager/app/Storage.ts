import { injectable } from "inversify";
import {ncp} from 'ncp';

@injectable()
export default class Storage {
  protected LESSON_BASE_DIR = `${__dirname}/lesson`
  protected PROJECT_BASE_DIR = `${__dirname}/project`

   copyDirectory(source: string, destination: string) {
    return new Promise((resolve, reject) => {
      ncp(source, destination, (err: Error) => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
}
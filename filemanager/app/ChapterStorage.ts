import { injectable } from "inversify";
import fs from  'fs';
import Git from 'nodegit';
import Storage from "./Storage";

@injectable()
export default class ChapterStorage extends Storage {
  async createChapter(revision: string, chapterId: string, branch: string, source: string) {
    const lessonDirectory = `${this.LESSON_BASE_DIR}/${revision}`
    const chapterDirectory = `${lessonDirectory}/chapter/${chapterId}`;
    await fs.promises.mkdir(chapterDirectory);

    // checkout chapter git
    const repository = await Git.Repository.open(lessonDirectory);
    //const commit = await repository.getCommit(commitHash);
    //await repository.checkoutBranch("master");
    //await Git.Checkout.tree(repository, commit, { checkoutStrategy: Git.Checkout.STRATEGY.FORCE});
    const branchReference = await repository.getBranch('refs/remotes/origin/' + branch);
    await repository.checkoutRef(branchReference);
    
    // coppy files
    await this.copyDirectory(`${lessonDirectory}/${source}`, chapterDirectory);
  }

  async getChapterContent(revision: string, chapterFile: string) {
    // reset content to master
    const lessonDirectory = `${this.LESSON_BASE_DIR}/${revision}`
    const repository = await Git.Repository.open(lessonDirectory);
    await repository.checkoutBranch("master");
    const masterComit = await repository.getReferenceCommit('master');
    await Git.Reset.reset(repository, masterComit, Git.Reset.TYPE.HARD, {});

    // clean other dir in path
    chapterFile.replace('..', '');
    chapterFile.replace('.', '');
    chapterFile.replace('/', '');

    // get chapter file
    const chapterContent = await fs.promises.readFile(`${this.LESSON_BASE_DIR}/${revision}/${chapterFile}.md`, 'utf8');
    return chapterContent;
  }

}


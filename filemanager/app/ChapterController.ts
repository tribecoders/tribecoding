import { injectable, inject } from 'inversify';
import { Request, Response} from 'express-serve-static-core';
import ChapterStorage from './ChapterStorage';
import LessonStorage from './LessonStorage';

@injectable()
export default class ChapterController {
  @inject(ChapterStorage) private chapterStorage!: ChapterStorage;
  @inject(LessonStorage) private lessonStorage!: LessonStorage;

  public async postChapter(req: Request, res: Response) {
    console.log('postChapter');
    const revision = req.params.revision;
    const chapterId = req.params.chapterId;
    const branch = req.body.branch;
    if (!revision || !chapterId || !branch) {
      res.send(false);
      return;
    }
    console.log(`POST /lessonRevision/${revision}/chapter/${chapterId}->${branch}`);
    try {
      const tribecoding = await this.lessonStorage.getLessonTribeCoding(revision);
      if (!tribecoding.source) {
        tribecoding.source = './src';
      }
      await this.chapterStorage.createChapter(revision, chapterId, branch, tribecoding.source);
      res.send(true);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async getChapter(req: Request, res: Response) {
    const revision = req.params.revision;
    const file = req.params.file;
    if (!revision || !file) {
      res.send(false);
      return;
    }
    console.log(`GET /lessonRevision/${revision}/chapter/${file}`);
    try {
      const content = await this.chapterStorage.getChapterContent(revision, file);
      res.send(content)
    } catch(err) {
      console.error(err);
      res.send(false);
    };
  }
}
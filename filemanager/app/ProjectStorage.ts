import { injectable } from "inversify";
import Storage from "./Storage";
import fs from  'fs';

@injectable()
export default class ProjectStorage extends Storage {

  async createProject(userId: string, lessonId: string, chapterId: string) {
    const projectDirectory = `${this.PROJECT_BASE_DIR}/${userId}/${lessonId}/${chapterId}`;
    if (!fs.existsSync(`${this.PROJECT_BASE_DIR}`)) {
      await fs.promises.mkdir(`${this.PROJECT_BASE_DIR}`);
    }
    if (!fs.existsSync(`${this.PROJECT_BASE_DIR}/${userId}/`)) {
      await fs.promises.mkdir(`${this.PROJECT_BASE_DIR}/${userId}/`);
    }
    if (!fs.existsSync(`${this.PROJECT_BASE_DIR}/${userId}/${lessonId}`)) {
      await fs.promises.mkdir(`${this.PROJECT_BASE_DIR}/${userId}/${lessonId}`);
    }
    if (!fs.existsSync(projectDirectory)) {
      fs.promises.mkdir(projectDirectory);
    }
    await this.copyDirectory(`${this.LESSON_BASE_DIR}/${lessonId}/chapter/${chapterId}`, projectDirectory);
  }

  async getProjectFiles(userId: string, lessonId: string, chapterId: string) {
    let directoryFiles = [];
    try {
      directoryFiles = await fs.promises.readdir(`${this.PROJECT_BASE_DIR}/${userId}/${lessonId}/${chapterId}`);
      return directoryFiles;
    } catch(err) {
      await this.createProject(userId, lessonId, chapterId);
      directoryFiles = await fs.promises.readdir(`${this.PROJECT_BASE_DIR}/${userId}/${lessonId}/${chapterId}`)
    }

    return directoryFiles;
  }

  async getProjectFile(userId: string, revision: string, chapterId: string, fileName: string) {
    const file = await fs.promises.readFile(`${this.PROJECT_BASE_DIR}/${userId}/${revision}/${chapterId}/${fileName}`, 'utf8');
    return file;
  }


  async setProjectFile(userId: string, revision: string, chapterId: string, fileName: string, content: string) {
      await fs.promises.writeFile(`${this.PROJECT_BASE_DIR}/${userId}/${revision}/${chapterId}/${fileName}`, content);
  }

  async restoreProjectFiles(userId: string, revision: string, chapterId: string) {
    const projectDirectory = `${this.PROJECT_BASE_DIR}/${userId}/${revision}/${chapterId}`;
    await this.copyDirectory(`${this.LESSON_BASE_DIR}/${revision}/chapter/${chapterId}`, projectDirectory);
  }

  async publish() {
    //npm install
    //queue for builds
    //copy files 
    //build dist
    //copy dist to published dir
    //make publish dir available to public
  }
}
import { injectable } from "inversify";
import fs from  'fs';
import rimraf from 'rimraf';
import Git from 'nodegit';
import { TribeCoding } from './TribeCoding';
import Storage from "./Storage";

@injectable()
export default class LessonStorage extends Storage  {
  async createLesson(revision: string, lessonGitUrl: string) {
    const lessonDirectory = `${this.LESSON_BASE_DIR}/${revision}`
    rimraf.sync(`${lessonDirectory}`);
    try{
      await Git.Clone.clone(lessonGitUrl, `${lessonDirectory}`);
    } catch (error) {
      console.error(`Clone error : ${error} possible private repository. Retry wih credentials.`);
      try {
        const credentials=`${process.env.GIT_LOGIN}:${process.env.GIT_PASS}`;
        const authLessonGitUrl =  lessonGitUrl.replace('https://', `https://${credentials}@`);
        console.log(authLessonGitUrl);
        await await Git.Clone.clone(authLessonGitUrl, `${lessonDirectory}`);
      } catch (error) {
        console.error(`Final clone error : ${error}`);
      }

    }
    await fs.promises.mkdir(`${lessonDirectory}/chapter`);
  }

  async getLessonTribeCoding(revision: string) {
    // reset content to master
    const lessonDirectory = `${this.LESSON_BASE_DIR}/${revision}`
    const repository = await Git.Repository.open(lessonDirectory);
    await repository.checkoutBranch("master");

    //read tribecoding
    let jsonString = "";
    try {
      jsonString = await fs.promises.readFile(`${lessonDirectory}/tribecoding.json`, 'utf8');
    } catch {
      jsonString = await fs.promises.readFile(`${lessonDirectory}/package.json`, 'utf8');
    }
    const tribecoding: TribeCoding = JSON.parse(jsonString);
    return tribecoding;
  }
}
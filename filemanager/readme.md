# About
Tribecoding file server

## Quick Start
To install this dependency use:

```
npm install
```
To run ther server in development mode:
```
npm run dev
```
set environment variable (NODE_ENV, GIT_LOGIN, GIT_PASS)
```
nano .env
```
## Run production
Run following command

```npm run prod```

build output will be available in **build** directory

## Build docker image

to build image run 

```
docker build -t lukaszbros/tribecoding_file .
```

to execute image (this will only start teh image without connecting to database container)

```
docker run -p 3001:3001 -d --name tribecoding_file lukaszbros/tribecoding_file 
```
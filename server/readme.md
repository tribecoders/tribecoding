# About
Tribecoding API server

## Quick Start
To install this dependency use:

```
npm install
```
To run ther server in development mode:
```
npm run dev
```
To run in dev mode .env file need to be established with folowing data:
```
TOKEN_SECRET=****
```

## Run production
Run following command

```npm run prod```

build output will be available in **build** directory

## Build docker image
to build image run 

```
docker build -t lukaszbros/tribecoding_server .
```

to execute image (this will only start teh image without connecting to database container)

```
docker run -p 3001:3001 -d --name tribecoding_server lukaszbros/tribecoding_server
```

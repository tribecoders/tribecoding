'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.createTable('user_chapter', {
    "user_id": { type: 'UUID', notNull: true},
    "lesson_id": {type: 'UUID', notNull: true}, 
    "chapter_no": 'int',
  }, callback);
};

exports.down = function(db) {
  return db.dropTable('user_chapter');
};

exports._meta = {
  "version": 1
};

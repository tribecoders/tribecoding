'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.createTable('user_credits', {
    "user_id": { type: 'UUID', notNull: true, unique: true},
    credits: { type: 'decimal', notNull: true, default: 0},
  }, callback);
};

exports.down = function(db, callback) {
  return db.dropTable('user_credits', callback);
};

exports._meta = {
  "version": 1
};

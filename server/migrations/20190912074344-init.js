'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.createTable('lesson', {
    id: { type: 'UUID', primaryKey: true, notNull: true},
    name: {type: 'string', unique: true},
    repository: {type: 'string', unique: true},
    tags: 'string',
    created: {type: 'datetime', defaultValue: new String('now()')}
  }, callback);
};

exports.down = function(db) {
  return db.dropTable('lesson');
};

exports._meta = {
  "version": 1
};

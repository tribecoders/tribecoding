'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.createTable('lesson_chapter', {
    id: { type: 'UUID', primaryKey: true, notNull: true},
    "lesson_revision": {type: 'UUID', notNull: true}, 
    name: 'string',
    "chapter_no": 'int',
    file: 'string'
  }, callback);
};

exports.down = function(db) {
  return db.dropTable('lesson_chapter');
};

exports._meta = {
  "version": 1
};

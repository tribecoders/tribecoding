'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  db.addColumn('user', 'is_teacher', {type: 'boolean', default: false}, () => {
    db.runSql('UPDATE "user" SET is_teacher = false', () => {
      db.changeColumn('user', 'is_teacher', {notNull: true}, callback);
    });
  });
};

exports.down = function(db, callback) {
  return db.removeColumn('user','is_teacher', callback);
};

exports._meta = {
  "version": 1
};

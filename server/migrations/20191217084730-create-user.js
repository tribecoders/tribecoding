'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.createTable('user', {
    id: { type: 'UUID', primaryKey: true, notNull: true},
    name: 'string',
    email: { type:'string', unique: true },
    password: 'string',
    "refresh_token": 'string',
    created: {type: 'datetime', defaultValue: new String('now()')}
  }, callback);
};

exports.down = function(db) {
  return db.dropTable('user');
};

exports._meta = {
  "version": 1
};

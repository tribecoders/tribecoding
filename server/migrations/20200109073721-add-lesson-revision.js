'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.createTable('lesson_revision', {
    'lesson_id': { type: 'UUID', notNull: true},
    revision: {type: 'string', unique: true},
    created: {type: 'datetime', defaultValue: new String('now()')}
  }, () => {
      db.renameColumn('lesson', 'revision', 'master_revision', callback);
    }
  );
};

exports.down = function(db, callback) {
  return db.dropTable('lesson_revision', () => {
    db.renameColumn('lesson', 'master_revision', 'revision', callback);
  });
};

exports._meta = {
  "version": 1
};

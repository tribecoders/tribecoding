'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.runSql('ALTER TABLE public.user_chapter ADD CONSTRAINT user_chapter_pkey PRIMARY KEY (user_id, lesson_id)', callback);
};

exports.down = function(db, callback) {
  return db.runSql('ALTER TABLE public.user_chapter DROP CONSTRAINT user_chapter_pkey', callback);
};

exports._meta = {
  "version": 1
};

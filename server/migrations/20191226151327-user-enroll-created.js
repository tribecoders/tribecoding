'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.addColumn('user_lesson','created', {type: 'datetime', defaultValue: new String('now()')},callback);
};

exports.down = function(db, callback) {
  return db.removeColumn('user_lesson','created', callback);
};

exports._meta = {
  "version": 1
};

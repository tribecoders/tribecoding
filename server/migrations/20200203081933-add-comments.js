'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.createTable('comment', {
    id: { type: 'UUID', primaryKey: true, notNull: true},
    'lesson_id': 'UUID',
    'chapter_id': 'UUID',
    position: 'string',
    'position_details': 'string',
    line: 'int',
    icon: 'string',
    'user_id': 'UUID',
    text: 'string', 
    created: {type: 'datetime', defaultValue: new String('now()')}
  }, () => {
    db.createTable('comment_response', {
      id: { type: 'UUID', primaryKey: true, notNull: true},
      'comment_id': 'UUID',
      'user_id': 'UUID',
      text: 'string', 
      created: {type: 'datetime', defaultValue: new String('now()')}
    },callback);
  });
};

exports.down = function(db, callback) {
  return db.dropTable('comment', () => {
    db.dropTable('comment_response', callback);
  });
};

exports._meta = {
  "version": 1
};

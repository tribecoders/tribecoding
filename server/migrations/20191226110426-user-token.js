'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.createTable('user_token', {
    "user_id": { type: 'UUID', notNull: true, unique: true},
    token: 'string',
  }, callback);
};

exports.down = function(db, callback) {
  return db.dropTable('user_token', callback);
};

exports._meta = {
  "version": 1
};

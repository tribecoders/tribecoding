import { Container } from 'inversify';
import Mailer from './Mailer';

export {default as stringToSlug} from './stringToSlug';
export {default as Mailer} from './Mailer';

const container = new Container();
container.bind(Mailer).toSelf();

export const utilsContainer = container;
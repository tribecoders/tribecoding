import nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';
import { injectable } from 'inversify';

@injectable()
export default class Mailer {
  private transport: Mail;

  constructor() {
    this.transport = nodemailer.createTransport({
      host: "jadecode.nazwa.pl",
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: process.env.EMAIL_LOGIN, // generated ethereal user
        pass: process.env.EMAIL_PASSWORD // generated ethereal password
      }
    });
  }  

  async sendMail(base64User: string, base64Time: string, hash:string) {
    try {
      const token = `${encodeURIComponent(base64User)}-${encodeURIComponent(base64Time)}-${encodeURIComponent(hash)}`
      let info = await this.transport.sendMail({
        from: `TribeCodding Support<${process.env.EMAIL_LOGIN}>`,
        to: "lukasz.bros@gmail.com",
        subject: "Recover password",
        text: `To recover password go to this link https://tribecoding.com/user/recover/${token}`,
        html: `To <b>recover</b> password go to this <a href="https://tribecoding.com/user/recover/${token}">link</a>`
      });

      console.log("Message sent: %s", info.messageId);
      return true;
    } catch (err) {
      console.error(err);
      return false;
    }
  }
}
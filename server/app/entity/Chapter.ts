export default class Chapter {
  id: string;
  lessonRevision: string;
  name: string;
  file: string;
  commit: string;
  chapterNo: number;

  constructor(id: string, lessonRevision:string, name:string, file: string, commit: string, chapterNo:number) {
    this.id = id;
    this.lessonRevision = lessonRevision;
    this.name = name;
    this.chapterNo = chapterNo;
    this.file = file;
    this.commit = commit
  }
}
export default class Position {
  lessonId: string;
  chapterId: string;
  position: string; //text, code, inline
  positionDetails: string; //file(code)
  line: number;

  constructor(data: any) {
    this.lessonId = data['lesson_id'];
    this.chapterId = data['chapter_id'];
    this.position = data['position'];
    this.positionDetails = data['position_details'];
    this.line = data['line'];
  }

  static fromUrl(data: any) {
    const position = new Position(data);
    position.lessonId = data['lessonId'];
    position.chapterId = data['chapterId'];
    position.positionDetails = data['positionDetails'];
    return position;
  }
}
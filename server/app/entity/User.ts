export default class User {
  id: string;
  name: string;
  email: string;
  isTeacher: boolean

  constructor(id: string, name:string, email: string, isTeacher: boolean = false) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.isTeacher = isTeacher;
  }

  getRaw() {
    return {
      id: this.id,
      name: this.name,
      email: this.email,
      isTeacher: this.isTeacher
    }
  }
}
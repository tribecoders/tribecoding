import Position from "./Position";

export default class Comment {
  id: string;
  position: Position;
  icon: string;
  userId: string;
  userName: string;
  text: string;

  constructor(data: any) {
    this.id = data['id'];
    this.position = new Position(data);
    this.icon = data['icon'];
    this.userId = data['user_id'];
    this.userName = data['user_name'];
    this.text = data['text'];
  }
}
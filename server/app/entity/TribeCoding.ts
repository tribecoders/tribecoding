export default interface TribeCoding {
  name: string;
  description: string;
  image: string;
  keywords: string[];
  source: string;
  chapters: TribeCodingChapter[];
}

export interface TribeCodingChapter {
  chapter: string;
  branch: string;
  file: string;
}

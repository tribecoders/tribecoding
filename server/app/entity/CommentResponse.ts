import Position from "./Position";

export default class CommentResponse {
  id: string;
  userId: string;
  userName: string;
  text: string;

  constructor(data: any) {
    this.id = data['id'];
    this.userId = data['user_id'];
    this.userName = data['user_name'];
    this.text = data['text'];
  }
}
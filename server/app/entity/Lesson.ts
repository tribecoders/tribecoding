import { multiBindToService } from "inversify";

export default class Lesson {
  id: string;
  name: string;
  description: string;
  slug: string;
  image: string;
  tags: string[];
  repository: string;
  price: number;
  revision: string;
  teacherId: string;
  created: string;

  constructor(data: any) {
    this.id = data['id'];
    this.name = data['name'];
    this.description = data['description'];
    this.slug = data['slug'];
    this.image = data['image'];
    this.tags = this.stringToArray(data['tags']);
    this.repository = data['repository'];
    this.price = data['price'] ? parseInt(data['price'].toString()) : 0;
    this.revision = data['master_revision'];
    this.teacherId = data['teacher_id'];
    this.created = data['created_string'];
  }

  stringToArray(data: string): string[] {
    if (data) {
      return data.split(',');
    } else {
      return [];
    }
  }
}
export default class UserCredits {
  credits: number;

  constructor(credits: number) {
    this.credits = credits;
  }
}
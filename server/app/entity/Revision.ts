export default class Revision {
  revision: string;
  created: string;

  constructor(data: any) {
    this.revision = data['revision'];
    this.created = data['created_string'];
  }
}
// lib/app.ts
import "reflect-metadata";
import express from 'express';
import bodyParser = require('body-parser');
import configureContainer from "./app.config";
import configureAuth from "./auth.config";
import { LessonController, ChapterController, UserController, CourseController, CourseFileController, CommentController } from "./controller";
import dotenv from 'dotenv';
import passport from "passport";
import {UserRepository} from "./repository";
import LessonAccess from "./LessonAccess";

dotenv.config();

// Create a new express application instance
const app: express.Application = express();

app.use(bodyParser.text());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const container = configureContainer();
const userController = container.get(UserController);
const lessonController = container.get(LessonController);
const chapterController = container.get(ChapterController);
const courseController = container.get(CourseController);
const courseFileController = container.get(CourseFileController);
const commentController = container.get(CommentController);
const lessonAccess = container.get(LessonAccess);

configureAuth(container.get(UserRepository));

// Authentication
app.post('/api/user/login', userController.postLogin.bind(userController));
app.post('/api/user', userController.postUser.bind(userController));
app.get('/api/user/token/:token', userController.getUserByToken.bind(userController));
app.post('/api/user/email', userController.validateEmail.bind(userController));
app.post('/api/user/request', userController.requestPassword.bind(userController));
app.put('/api/user/recover', userController.recoverPassword.bind(userController));
app.get('/api/user/credits', passport.authenticate('jwt', { session: false }), userController.getUserCredits.bind(userController));

// Lesson
app.post('/api/lesson', passport.authenticate('jwt', {session: false}), lessonController.postLesson.bind(lessonController));
app.get('/api/lesson', lessonController.getLessons.bind(lessonController));
app.put('/api/lesson/:lessonId/revision', passport.authenticate('jwt', {session: false}), lessonController.putLessonRevision.bind(lessonController));
app.get('/api/lesson/:lessonId', lessonController.getLesson.bind(lessonController));
app.get('/api/lesson/slug/:slug', lessonController.getLessonBySlug.bind(lessonController));
app.get('/api/lesson/:lessonId/author', lessonController.getLessonAuthor.bind(lessonController));
app.get('/api/lesson/:lessonId/revision', lessonController.getLessonRevision.bind(lessonController));


// Course chapter
app.get('/api/lessonRevision/:revision/chapter', chapterController.getChapters.bind(chapterController));
app.get('/api/lessonRevision/:revision/chapter/:chapterId/content', (req, res) => {
  lessonAccess.withLessonContentAccess(req, res, chapterController.getChapterContent.bind(chapterController));
});

// Course file
app.get('/api/lessonRevision/:revision/chapter/:chapterId/file', courseFileController.getProjectFiles.bind(courseFileController));
app.get('/api/lessonRevision/:revision/chapter/:chapterId/file/:fileName', (req, res) => {
  lessonAccess.withLessonContentAccess(req, res, courseFileController.getProjectFile.bind(courseFileController));
});

// Course 
app.post('/api/lesson/slug/:slug/enroll', passport.authenticate('jwt', {session: false}), courseController.enrollLesson.bind(courseController));
app.get('/api/user/lesson', passport.authenticate('jwt', {session: false}), courseController.getEnrolledLessons.bind(courseController));
app.post('/api/user/lesson/:lessonId/chapter', passport.authenticate('jwt', {session: false}), courseController.postUserChapter.bind(courseController));
app.get('/api/user/lesson/:lessonId/chapter', passport.authenticate('jwt', {session: false}), courseController.getUserChapter.bind(courseController));

// Comment
app.post('/api/comment', passport.authenticate('jwt', {session: false}), commentController.postComment.bind(commentController));
app.get('/api/comment/lesson/:lessonId/chapter/:chapterId', commentController.getComments.bind(commentController));
app.post('/api/comment/:commentId/response', passport.authenticate('jwt', {session: false}), commentController.postCommentResponse.bind(commentController));
app.get('/api/comment/:commentId/response', commentController.getCommentsResponse.bind(commentController));

//Start app
app.listen(3001, function () {
	console.log('Example app listening on port 3001!');
});
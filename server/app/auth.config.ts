import passport from 'passport'
import passportJWT from 'passport-jwt'
import passportLocal from 'passport-local';
import UserRepository from './repository/UserRepository';
import User from './entity/User';

export default function configureAuth(userRepository: UserRepository) {
  const ExtractJWT = passportJWT.ExtractJwt;
  const LocalStrategy = passportLocal.Strategy;
  const JWTStrategy   = passportJWT.Strategy;

  passport.use(new JWTStrategy({
      jwtFromRequest: ExtractJWT.fromHeader('authorization'),
      secretOrKey   : process.env.TOKEN_SECRET
    },
    function (jwtPayload: User, cb) {
    //find the user in db if needed
    return userRepository.selectUser(jwtPayload.id)
      .then(user => {
          if (user) {
            return cb(null, user.getRaw());
          } else {
            cb(false);
          }
      })
      .catch(err => {
        console.error(err);
        return cb(err);
      });
    }
  ));

  passport.use(new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password'
    },
    function (email, password, cb) {
      return (userRepository.selectByEmailAndPassword(email, password))
        .then(user => {
          if (!user) {
            return cb(null, undefined, {message: 'Incorrect email or password.'});
          }
          return cb(null, user.getRaw(), {
            message: 'Logged In Successfully'
          });
        })
        .catch(err => {
          console.error(err);
            return cb(err);
        });
    }
  ));
}
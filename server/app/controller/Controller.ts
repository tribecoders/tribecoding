import { injectable } from "inversify";
import fetch from "node-fetch";

@injectable()
export default class Controller {
  
  async fileServer(path: string, params?: object) {
    let serverPath = 'http://localhost:3002';
    if (process.env.NODE_ENV === 'production') {
      serverPath = 'http://file_tribecoding:3002';
    }
    if (params) {
      return await fetch(serverPath + path, params);
    } else {
      return await fetch(serverPath + path);
    }
  }

  getErrorsMessage(message: string, param?: string) {
    return { errors:
      [{
        msg: message,
        param
      }],
    }
  }
}
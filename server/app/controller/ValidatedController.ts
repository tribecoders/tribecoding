import { validationResult } from "express-validator";
import { Request, Response} from 'express-serve-static-core';
import { injectable } from "inversify";
import Controller from "./Controller";

@injectable()
export default class ValidatedController extends Controller {
  protected validate(req: Request, res: Response): boolean {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(422).json({ errors: errors.array() });
      return false;
    }

    return true;
  }
}
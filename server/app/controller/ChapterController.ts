import { injectable, inject } from 'inversify';
import { Request, Response} from 'express-serve-static-core';
import { ChapterRepository } from '../repository';
import Controller from './Controller';

@injectable()
export default class ChapterController extends Controller {
  @inject(ChapterRepository) private chapterRepository!: ChapterRepository;

  public async getChapters(req: Request, res: Response) {
    const revision = req.params.revision;
    if (!revision) {
      res.send(false);
      return;
    }
    console.log(`GET /lessonRevision/${revision}/chapter`);
    try {
      const chapters = await this.chapterRepository.selectChapters(revision);
      res.send(chapters);
    } catch(err) {
      console.error(err);
      res.send(false);
    };
  }

  public async getChapterContent(req: Request, res: Response) {
    const revision = req.params.revision;
    const chapterId = req.params.chapterId;
    if (!revision || !chapterId) {
      res.send(false);
      return;
    }
    
    console.log(`GET /lessonRevision/${revision}/chapter/${chapterId}/content`);
    
    try {
      const chapter = await this.chapterRepository.selectChapter(revision, chapterId);
      const response = await this.fileServer(`/lessonRevision/${revision}/chapter/${chapter.file}`);
      const content = await response.text();
      res.send(content);
    } catch(err) {
      console.error(err);
      res.send(false);
    };
  }
}
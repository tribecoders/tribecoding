import passport from 'passport';
import jwt from 'jsonwebtoken';
import { injectable, inject } from "inversify";
import { Request, Response} from 'express-serve-static-core';
import { UserRepository } from '../repository';
import {User} from '../entity';
import {body, sanitizeBody} from "express-validator";
import ValidatedController from './ValidatedController';
import uuid from 'node-uuid';
import { Mailer } from '../utils';
import CryptoJS from 'crypto-js'

@injectable()
export default class UserController extends ValidatedController {
  @inject(UserRepository) private userRepository!: UserRepository;
  @inject(Mailer) private mailer!: Mailer;
  private timeout = 1000*60*10;

  public async postLogin(req: Request, res: Response) {
    await body("email", "Email is not valid").isEmail().run(req);
    await body("password", "Password cannot be empty").isLength({min: 1}).run(req);
    sanitizeBody("email");
    if (!this.validate(req, res)) return;

    console.log('POST /login');
    passport.authenticate('local', {session: false}, (err: string, user: User, info) => {
      if (err || !user) {
        if (err) {
          console.error(err);
        }
        return res.json({
            errors: [{msg: info ? info.message : 'Login failed'}]
        });
      }

      req.login(user, {session: false}, (err) => {
          if (err) {
              console.error(err);
              res.send(false);
          }

          const secret =  process.env.TOKEN_SECRET;

          if (secret) {
            const token = jwt.sign(user,  secret);
            this.userRepository.updateOrInsertUserToken(user.id, token);
            return res.json({user, token});
          } else {
            res.send(false);
          }
          
      });
    })(req, res)
  }

  public async postUser(req: Request, res: Response) {  
    await body("name", "Name cannot be empty").isLength({min: 1}).run(req)
    await body("email", "Email is not valid").isEmail().run(req);
    await body("password", "Password have to be atleast 6 characters long").isLength({min: 6}).run(req);
    await sanitizeBody("name");
    await sanitizeBody("email");
    if (!this.validate(req, res)) return;
    const {name, email, password} = req.body;

    console.log('POST /user');

    try {
      let userId = uuid.v4();
      await this.userRepository.insertUser(userId, name, email, password);
      res.send(true);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async getUserByToken(req: Request, res: Response) {
    const {token} = req.params;
    if (!token) {
      res.send(false);
    }

    console.log(`POST /user/token/${token}`);

    try {
      const user = await this.userRepository.selectUserByToken(token);
      res.send(user);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async validateEmail(req: Request, res: Response) {
    await body("email", "Email is not valid").isEmail().run(req);
    await sanitizeBody("email");
    if (!this.validate(req, res)) return;
    const {email} = req.body;
  
    console.log(`POST /user/email => ${email}`);

    try {
      const valid = await this.userRepository.validateEmail(email);
      if (valid) {
        res.send(valid);
      } else {
        res.json(this.getErrorsMessage('Email already in use!', 'email'));
      }
    } catch (err) {
      res.send(false);
    }
  }

  public async getUserCredits(req: Request, res: Response) {
    try {
        const user: User | undefined = req.user as any;
        console.log(`GET /user/credits`);
        if (user) {
          const credits = await this.userRepository.selectUserCredits(user.id);
          res.send(credits);
        } else {
          res.send(false);
        }
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async requestPassword(req: Request, res: Response) {
    await body("email", "Email is not valid").isEmail().run(req);
    sanitizeBody("email");
    if (!this.validate(req, res)) return;

    const {email} = req.body;

    console.log(`POST /user/request-> ${email}`);

    try {
      const user = await this.userRepository.selectUserByEmail(email);
      if (!user) {
        res.json(this.getErrorsMessage('User does not exist', 'email'));
        return;
      }
      const wordUser = CryptoJS.enc.Utf8.parse(user.id);
      const base64User = CryptoJS.enc.Base64.stringify(wordUser)

      const currentDate = new Date();
      const time = currentDate.getTime();
      const wordTime = CryptoJS.enc.Utf8.parse(time.toString());
      const base64Time = CryptoJS.enc.Base64.stringify(wordTime);

      const message = `${Math.floor(time/this.timeout)}${user.id}${user.email}${user.email}`;
      const encrypted = CryptoJS.HmacSHA1(message, process.env.EMAIL_SECRET);
      const hash = CryptoJS.enc.Base64.stringify(encrypted);

      const success = await this.mailer.sendMail(base64User, base64Time, hash);
      res.send(success);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async recoverPassword(req: Request, res: Response) {
    await body("password", "Password have to be atleast 6 characters long").isLength({min: 6}).run(req);
    sanitizeBody("timeBase64");
    sanitizeBody("userBase64");
    sanitizeBody("hash");
    let {timeBase64, userBase64, hash, password} = req.body;
    timeBase64 = decodeURIComponent(timeBase64);
    userBase64 = decodeURIComponent(userBase64);
    hash = decodeURIComponent(hash);


    if (!timeBase64 || !userBase64 || !hash || !password) {
      res.send(false);
      return;
    }

    console.log(`POST /user/recover -> ${timeBase64}, ${userBase64}, ${hash}`);

    try {
      // Verify request time
      const decodedTime = CryptoJS.enc.Base64.parse(timeBase64);
      const time = parseInt(CryptoJS.enc.Utf8.stringify(decodedTime));
      const currentTime = (new Date()).getTime();
      if (currentTime - this.timeout > time) {
        res.json(this.getErrorsMessage('Password request timeout!'));
        return;
      }

      console.log(time);

      // Verify user
      const decodedUser = CryptoJS.enc.Base64.parse(userBase64);
      const userId = CryptoJS.enc.Utf8.stringify(decodedUser);
      const user = await this.userRepository.selectUser(userId);
      if (!user) {
        res.json(this.getErrorsMessage('User do not exist!'));
        return;
      }

      console.log(user);

      // verify hash
      const verifyMessage = `${Math.floor(time/this.timeout)}${user.id}${user.email}${user.email}`;
      const verifyEncrypted = CryptoJS.HmacSHA1(verifyMessage, process.env.EMAIL_SECRET);
      const verifyHash = CryptoJS.enc.Base64.stringify(verifyEncrypted);
      const sucess = verifyHash === hash;
      
      if (!sucess) {
        res.json(this.getErrorsMessage('Verification hash failure!'));
        return;
      }

      // update to new password
      await this.userRepository.updateUserPassword(user.id, password);

      res.send(true);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }
}
import { Container } from 'inversify';
import LessonController from './LessonController';
import ChapterController from './ChapterController';
import CourseController from './CourseController';
import CourseFileController from './CourseFileController';
import UserController from './UserController';
import CommentController from './CommentController';

export {default as LessonController} from './LessonController';
export {default as ChapterController} from './ChapterController';
export {default as CourseController} from './CourseController';
export {default as CourseFileController} from './CourseFileController';
export {default as UserController} from './UserController';
export {default as CommentController} from './CommentController';

const container = new Container();
container.bind(LessonController).toSelf();
container.bind(ChapterController).toSelf();
container.bind(CourseController).toSelf();
container.bind(CourseFileController).toSelf();
container.bind(UserController).toSelf();
container.bind(CommentController).toSelf();

export const controllerContainer = container;
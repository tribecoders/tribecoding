import { Request, Response} from 'express-serve-static-core';
import { injectable, inject } from "inversify";
import {ChapterRepository, LessonRepository} from '../repository';
import uuid from 'node-uuid';
import { TribeCoding, Chapter, User } from '../entity';
import Controller from './Controller';
import { stringToSlug } from '../utils';

@injectable()
export default class LessonController extends Controller {
  @inject(LessonRepository) private lessonRepository!: LessonRepository;
  @inject(ChapterRepository) private chapterRepository!: ChapterRepository;
  
  public async postLesson(req: Request, res: Response) {
    const user: User | undefined = req.user as any;
    const lessonGitUrl = req.body.repository;
    if (!lessonGitUrl || !user) {
      res.send(false);
      return;
    }
    
    console.log(`POST /lesson -> ${lessonGitUrl}`);
    try {
      let revision = uuid.v4();
    
      // Populate lesson repository
      await this.fileServer(`/lessonRevision/${revision}`, { 
        method: 'POST',  
        headers: { 'Content-Type': 'application/json' },
        body:JSON.stringify({ repository: lessonGitUrl })  
      });
      const response = await this.fileServer(`/lessonRevision/${revision}`);
      const tribecoding: TribeCoding = await response.json();
      if (!tribecoding) {
        console.error('Error posting lesson files!');
        res.send(false);
        return;
      }
      // Change name
      tribecoding.name = tribecoding.name[0].toUpperCase() +  
      tribecoding.name.slice(1).replace('-', ' ').replace('_', ' ');
      // Add defaults
      if (!tribecoding.image) {
        tribecoding.image = "/images/code.jpg";
      }
      if (!tribecoding.source) {
        tribecoding.source = "./src";
      }

      // Insert data to database

      let lessonId = uuid.v4();
      const lessonSlug = stringToSlug(tribecoding.name.toLowerCase());
      // Check for previously inserted lesson
      const lesson = await this.lessonRepository.selectLessonBySlug(lessonSlug);
      if (lesson) {
        if(lesson.teacherId !== user.id) {
          console.error('Error not a lesson owner');
          res.send(false);
          return;
        }
        lessonId = lesson.id
        await this.lessonRepository.updateLesson(lessonId, tribecoding);
        await this.lessonRepository.insertLessonRevision(lessonId, revision);
      } else {
        await this.lessonRepository.insertLesson(lessonId, lessonSlug, tribecoding, lessonGitUrl, user);
        await this.lessonRepository.insertLessonRevision(lessonId, revision);
      }
      

      let index = 0;
      if (!tribecoding.chapters) {
        tribecoding.chapters = [{
          chapter: "",
          branch: "master",
          file: "README"
        }]
      }
      for (const tribecodingChapter of tribecoding.chapters) {
        const chapterId = uuid.v4();
        const chapter = new Chapter(chapterId, revision, tribecodingChapter.chapter, tribecodingChapter.file, tribecodingChapter.branch, index++);
        // Populate files
        await this.fileServer(`/lessonRevision/${revision}/chapter/${chapterId}`, { 
          method: 'POST',  
          headers: { 'Content-Type': 'application/json' },
          body:JSON.stringify({ branch: tribecodingChapter.branch })
        });
      
        // Insert data to database
        await this.chapterRepository.inserChapter(chapter);
      }

      //Auto publish
      console.error(`autopublih ${lessonId} ${revision}`);
      this.lessonRepository.updateLessonMasterRevsion(lessonId, revision);
      
      res.send({
        lessonSlug: lessonSlug,
        revision: revision
      
      });
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async getLesson(req: Request, res: Response) {
    const lessonId = req.params.lessonId;
    if (!lessonId) {
      res.send(false);
      return;
    }
    console.log(`GET /lesson/${lessonId}`);
    try {
      const lesson = await this.lessonRepository.selectLesson(lessonId);
      res.send(lesson);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async getLessonBySlug(req: Request, res: Response) {
    const slug = req.params.slug;
    if (!slug) {
      res.send(false);
      return;
    }
    console.log(`GET /lesson/slug/${slug}`);
    try {
      const lesson = await this.lessonRepository.selectLessonBySlug(slug);
      res.send(lesson);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async getLessons(req: Request, res: Response) {
    console.log(`GET /lesson`);
    try {
      const lessons = await this.lessonRepository.selectLessons();
      res.send(lessons);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async getLessonAuthor(req: Request, res: Response) {
    const lessonId = req.params.lessonId;
    if (!lessonId) {
      res.send(false);
      return;
    }
    console.log(`GET /lesson/${lessonId}/author`);
    try {
      const teacher = await this.lessonRepository.selectLessonAuthor(lessonId);
      res.send(teacher);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async putLessonRevision(req: Request, res: Response) {
    const user: User | undefined = req.user as any;
    const lessonId = req.params.lessonId;
    const revision = req.body.revision;
    if (!lessonId || !revision || !user) {
      res.send(false);
      return;
    }
    const teacher = await this.lessonRepository.selectLessonAuthor(lessonId);
    if(teacher.id !== user.id) {
      console.error('Error not a lesson owner');
      res.send(false);
      return;
    }
    console.log(`PUT /lesson/${lessonId} -> ${revision}`);
    try {
      await this.lessonRepository.updateLessonMasterRevsion(lessonId, revision);
      res.send(true);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async getLessonRevision(req: Request, res: Response) {
    const lessonId = req.params.lessonId;
    if (!lessonId) {
      res.send(false);
      return;
    }
    console.log(`GET /lesson/${lessonId}/revision`);
    try {
      const revision = await this.lessonRepository.selectLessonRevision(lessonId);
      res.send(revision);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }
}
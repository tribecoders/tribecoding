import Controller from "./Controller";
import { Request, Response} from 'express-serve-static-core';
import { User } from "../entity";
import { inject, injectable } from "inversify";
import { LessonRepository, UserRepository, CourseRepository } from "../repository";

@injectable()
export default class CourseController extends Controller {
  @inject(LessonRepository) private lessonRepository!: LessonRepository;
  @inject(UserRepository) private userRepository!: UserRepository;
  @inject(CourseRepository) private courseRepository!: CourseRepository;

  public async getEnrolledLessons(req: Request, res: Response) {
    const user: User | undefined = req.user as any;
    if (!user) {
      res.send(false);
      return;
    }
    console.log(`GET /user/lesson`);
    try {
      const lessons = await this.courseRepository.selectEnrolledLessons(user.id);
      res.send(lessons);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async enrollLesson(req: Request, res: Response) {
    const slug = req.params.slug;
    const user: User | undefined = req.user as any;
    
    if (!user || !slug) {
      res.send(false);
      return;
    }

    console.log(`POST /api/lesson/slug/${slug}/enroll`);
    
    try {
      const lesson = await this.lessonRepository.selectLessonBySlug(slug);
      const isEnrolled = await this.courseRepository.isEnrolled(user.id, lesson.id);
      if (!isEnrolled) {
        const userCredits = await this.userRepository.selectUserCredits(user.id);
        if (userCredits.credits >= lesson.price) {
          await this.courseRepository.enrollLesson(user.id, lesson.id, lesson.price);
          res.send(true);
        } else {
          res.send({errors:[{
            msg: 'Not enough credits!'
          }]});
        }
      } else {
        res.send(true);
      }
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async postUserChapter(req: Request, res: Response) {
    const user: User | undefined = req.user as any;
    const lessonId = req.params.lessonId;
    const chapterNo = req.body.chapterNo;

    if (!lessonId || !chapterNo || !user) {
      res.send(false);
      return;
    }
    console.log(`POST /user/lesson/${lessonId}/chapter -> ${chapterNo}`);
    try {
      await this.courseRepository.updateUserChapter(user.id, lessonId, chapterNo);
      res.send(true);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async getUserChapter(req: Request, res: Response) {
    const user: User | undefined = req.user as any;
    const lessonId = req.params.lessonId;

    if (!user || !lessonId) {
      res.send(false);
      return;
    }
    console.log(`GET /user/lesson/${lessonId}/chapter`);
    try {
      const chapterNo = await this.courseRepository.selectUserChapter(user.id, lessonId);
      res.send({chapterNo: chapterNo});
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }
}
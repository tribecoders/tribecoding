import { injectable } from 'inversify';
import { Request, Response } from 'express-serve-static-core';
import Controller from './Controller';

@injectable()
export default class CourseFileController extends Controller {

  async getProjectFiles(req: Request, res: Response) {
    const revision = req.params.revision;
    const chapterId = req.params.chapterId;
    if (!revision || !chapterId) {
      res.send(false);
      return;
    }
    console.log(`GET /lessonRevision/${revision}/chapter/${chapterId}/file`)
    try {
      const response = await this.fileServer(`/lessonRevision/${revision}/chapter/${chapterId}/file`);
      const files = await response.json();
      res.send(files);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  async getProjectFile(req: Request, res: Response) {
    const revision = req.params.revision;
    const chapterId = req.params.chapterId;
    const fileName = req.params.fileName;
    if (!revision || !chapterId || !fileName) {
      res.send(false);
      return;
    }
    console.log(`GET /lessonRevision/${revision}/chapter/${chapterId}/file/${fileName}`);
    try {
      const response = await this.fileServer(`/lessonRevision/${revision}/chapter/${chapterId}/file/${fileName}`);
      const file = await response.text();
      res.send(file);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }
  
  async putProjectFile(req: Request, res: Response) {
    const lessonId = req.params.lessonId;
    const chapterId = req.params.chapterId;
    const fileName = req.params.fileName;
    const content = req.body;
    if (!lessonId || !chapterId || !fileName) {
      res.send(false);
      return;
    }
    console.log(`PUT /lesson/${lessonId}/chapter/${chapterId}/file/${fileName}`)
    try {
      await this.fileServer(`/lessonRevision/${lessonId}/chapter/${chapterId}/file/${fileName}`, { 
        method: 'PUT',
        body: content
      });
      res.send(true);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  async putProjectFiles(req: Request, res: Response) {
    const lessonId = req.params.lessonId;
    const chapterId = req.params.chapterId
    if (!lessonId || !chapterId) {
      res.send(false);
      return;
    }
    console.log(`PUT /lesson/${lessonId}/chapter/${chapterId}/file`)
    try {
      await this.fileServer(`/lessonRevision/${lessonId}/chapter/${chapterId}/file`, { 
        method: 'PUT'
      });
      res.send(true);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }
}
import Controller from "./Controller";
import { Request, Response} from 'express-serve-static-core';
import { User } from "../entity";
import { inject, injectable } from "inversify";
import { CommentRepository } from "../repository";
import Position from "../entity/Position";
import { sanitizeBody } from "express-validator";
import uuid from "node-uuid";

@injectable()
export default class CommentController extends Controller {
  @inject(CommentRepository) private commentRepository!: CommentRepository;

  public async postComment(req: Request, res: Response) {
    const position = Position.fromUrl(req.body.position);
    const icon = req.body.icon;
    const text = req.body.text;
    sanitizeBody('text');
    const user: User | undefined = req.user as any;
    if (!text || !user || !position) {
      res.send(false);
      return;
    }
    console.log(`POST /comment`);
    try {
      let commentId = uuid.v4();
      await this.commentRepository.insertCommment(commentId, position, icon, user.id, text);
      res.send(true);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async getComments(req: Request, res: Response) {
    const lessonId = req.params.lessonId;
    const chapterId = req.params.chapterId;
    if (!lessonId || !chapterId) {
      res.send(false);
      return;
    }

    console.log(`GET /comment`);
    try {
      const comments = await this.commentRepository.selectComments(lessonId, chapterId);
      res.send(comments);
    } catch (err) {
      console.error(err);
      res.send([]);
    }
  }

  public async postCommentResponse(req: Request, res: Response) {
    const commentId = req.params.commentId
    const text = req.body.text;
    sanitizeBody('text');
    const user: User | undefined = req.user as any;
    if (!text || !commentId || !user ) {
      res.send(false);
      return;
    }
    console.log(`POST /comment/${commentId}/response`);
    try {
      let commentResponseId = uuid.v4();
      await this.commentRepository.insertCommentResponse(commentResponseId, commentId, user.id, text);
      res.send(true);
    } catch (err) {
      console.error(err);
      res.send(false);
    }
  }

  public async getCommentsResponse(req: Request, res: Response) {
    const commentId = req.params.commentId
    if (!commentId) {
      res.send(false);
      return;
    }

    console.log(`GET /comment/${commentId}/response`);
    try {
      const commentResponses = await this.commentRepository.selectCommentResponses(commentId);
      res.send(commentResponses);
    } catch (err) {
      console.error(err);
      res.send([]);
    }
  }
}
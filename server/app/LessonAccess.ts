import { injectable, inject } from "inversify";
import { LessonRepository, CourseRepository } from "./repository";
import { User, Lesson } from "./entity";
import { Request, Response } from 'express-serve-static-core';
import passport = require("passport");

@injectable()
export default class LessonAccess {
  @inject(LessonRepository) protected lessonRepository!: LessonRepository;
  @inject(CourseRepository) protected courseRepository!: CourseRepository;

  async withLessonContentAccess(req: Request, res: Response, getContent: (req: Request, res: Response) => void) {
    const revision = req.params.revision;
    if (!revision) {
      res.status(404).send(false);
    }
    try {
      const lesson = await this.lessonRepository.selectLessonByRevision(revision);
      if (!lesson) {
        return res.status(404).send(false);
      }

      if (req.headers.authorization) {
        passport.authenticate('jwt', {session: false}, (err, user) => {
          if (user) {
              req.user = user;
              this.verifyUserAccess(lesson, user).then(success => {
                if (success) {
                  return getContent(req, res);
                } else {
                  return res.status(403).send('Unauthorised');
                }
              });
          } else {
            return res.status(403).send('Unauthorised');
          }
        })(req, res);
      } else {
        if (this.verifyGuestAccess(lesson)) {
          return getContent(req, res);
        }
        return res.status(403).send('Unauthorised');
      }
    } catch (err) {
      console.error(err);
      res.status(404).send(false);
    }
  }

  verifyGuestAccess(lesson: Lesson) {
    return this.isPublic(lesson)
  }

  async verifyUserAccess(lesson: Lesson, user: User) {
    if (this.isOwner(lesson, user)) {
      return true;
    } else {
      if (this.isPublic(lesson)) {
        return true;
      }
      const isEnroled = await this.courseRepository.isEnrolled(user.id, lesson.id);
      return isEnroled;
    }
  }

  private isPublic(lesson: Lesson): boolean {
    return !lesson.price;
  }

  private isOwner(lesson: Lesson, user: User): boolean{
    return lesson.teacherId === user.id;
  }
}
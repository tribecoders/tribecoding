
import { controllerContainer}  from './controller';
import { Container } from "inversify";
import { repositoryContainer } from './repository';
import { utilsContainer } from './utils';
import LessonAccess from './LessonAccess';
// Disable inversify logger middleware
// import { makeLoggerMiddleware } from 'inversify-logger-middleware';

export default function configureContainer() {
  const container = Container.merge(Container.merge(controllerContainer, repositoryContainer), utilsContainer);
  container.bind(LessonAccess).toSelf();
  //let logger = makeLoggerMiddleware();
  // container.applyMiddleware(logger);
  return container;
}
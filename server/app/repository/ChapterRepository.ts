import { injectable } from "inversify";
import Persisted from "./Persisted";
import {Chapter} from "../entity";

@injectable()
export default class ChapterRepository extends Persisted {
  public async inserChapter(chapter: Chapter) {
    const query = 'INSERT INTO lesson_chapter(id, lesson_revision, name, file, commit, chapter_no) VALUES($1, $2, $3, $4, $5, $6)';
    await this.database.makeQuery(query, [chapter.id, chapter.lessonRevision, chapter.name, chapter.file, chapter.commit, chapter.chapterNo])
  }

  public async selectChapters(lessonRevision: string): Promise<Chapter[]> {
    const query = 'SELECT id, lesson_revision, name, file, commit, chapter_no FROM lesson_chapter WHERE lesson_revision = $1';
    const dbResult = await this.database.makeQuery(query, [lessonRevision]);
    const chapters = dbResult.rows.map(row => new Chapter(row['id'], row['lesson_revision'], row['name'], row['file'], row['commit'], row['chapter_no']));
    return chapters;
  }

  public async selectChapter(lessonRevision: string, chapterId: string): Promise<Chapter> {
    const query = 'SELECT id, lesson_revision, name, file, commit, chapter_no FROM lesson_chapter WHERE lesson_revision = $1 AND id = $2';
    const dbResult = await this.database.makeQuery(query, [lessonRevision, chapterId]);
    const chapter = dbResult.rows.map(row => new Chapter(row['id'], row['lesson_revision'], row['name'], row['file'], row['commit'], row['chapter_no']))[0];
    return chapter;
  }
}
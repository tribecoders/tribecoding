import { injectable } from "inversify";
import Persisted from "./Persisted";
import Lesson from "../entity/Lesson";
import { TribeCoding, User, Teacher } from "../entity";
import Revision from "../entity/Revision";

@injectable()
export default class LessonRepository extends Persisted {
  public static LESSON_DIRECTORY = `${__dirname}/lesson`

  public async insertLesson(lessonId: string, slug:string, tribecoding: TribeCoding, gitUrl: string, user: User) {
    const query = 'INSERT INTO lesson(id, name, description, slug, image, tags, repository, teacher_id, price) VALUES($1, $2, $3, $4, $5, $6, $7, $8, 0)';
    await this.database.makeQuery(query, [lessonId, tribecoding.name, tribecoding.description, slug, tribecoding.image, this.arrayToString(tribecoding.keywords), gitUrl, user.id]);
  }

  public async updateLesson(lessonId: string, tribecoding: TribeCoding) {
    const nameQuery = 'UPDATE lesson SET name=$2 WHERE id = $1 AND name != $2';
    await this.database.makeQuery(nameQuery, [lessonId, tribecoding.name]);
    const query = 'UPDATE lesson SET description=$2, image=$3, tags=$4 WHERE id = $1';
    await this.database.makeQuery(query, [lessonId, tribecoding.description, tribecoding.image, this.arrayToString(tribecoding.keywords)]);
  }

  public async insertLessonRevision(lessonId: string, revision: string) {
    const query = 'INSERT INTO lesson_revision(lesson_id, revision) VALUES($1, $2)';
    await this.database.makeQuery(query, [lessonId, revision]);
  }

  public async selectLesson(lessonId: string): Promise<Lesson> {
    const query = 'SELECT *, TO_CHAR(created, \'DD Mon YYYY\') as created_string  FROM lesson WHERE id = $1';
    const dbResult = await this.database.makeQuery(query, [lessonId]);
    const lesson = dbResult.rows.map(row => new Lesson(row))[0];
    return lesson;
  }

  public async selectLessonBySlug(slug: string): Promise<Lesson> {
    const query = 'SELECT *, TO_CHAR(created, \'DD Mon YYYY\') as created_string FROM lesson WHERE slug = $1 ORDER BY created DESC';
    const dbResult = await this.database.makeQuery(query, [slug]);
    const lesson = dbResult.rows.map(row => new Lesson(row))[0];
    return lesson;
  }

  public async selectLessonByRevision(revision: string): Promise<Lesson> {
    const query = 'SELECT l.*, TO_CHAR(l.created, \'DD Mon YYYY\') as created_string FROM lesson l JOIN lesson_revision lr ON l.id = lr.lesson_id WHERE lr.revision = $1 ORDER BY l.created DESC';
    const dbResult = await this.database.makeQuery(query, [revision]);
    const lesson = dbResult.rows.map(row => new Lesson(row))[0];
    return lesson;
  }

  public async selectLessons(): Promise<Lesson[]> {
    const query = 'SELECT *, TO_CHAR(created, \'DD Mon YYYY\') as created_string FROM lesson WHERE price >= 0';
    const dbResult = await this.database.makeQuery(query, []);
    const lessons = dbResult.rows.map(row => new Lesson(row));
    return lessons;
  }
  
  public async updateLessonMasterRevsion(lessonId: string, revision: string): Promise<void> {
    const query = 'UPDATE lesson SET master_revision = $2 WHERE id = $1';
    await this.database.makeQuery(query, [lessonId, revision]);
  }

  public async selectLessonAuthor(lessonId: string): Promise<Teacher> {
    const query = 'SELECT u.id, u.name FROM lesson l JOIN "user" u ON l.teacher_id = u.id WHERE l.id = $1';
    const dbResult = await this.database.makeQuery(query, [lessonId]);
    const teacher = dbResult.rows.map(row => new Teacher(row))[0];
    return teacher;
  }
  public async selectLessonRevision(lessonId: string): Promise<Revision> {
    const query = 'SELECT lr.*, TO_CHAR(lr.created, \'DD Mon YYYY\') as created_string FROM lesson l JOIN lesson_revision lr ON l.master_revision = lr.revision WHERE l.id = $1';
    const dbResult = await this.database.makeQuery(query, [lessonId]);
    const revision = dbResult.rows.map(row => new Revision(row))[0];
    return revision;
  }
}
import { Container } from 'inversify';
import Database from './Database';
import LessonRepository from './LessonRepository';
import ChapterRepository from './ChapterRepository';
import UserRepository from './UserRepository';
import CourseRepository from './CourseRepository';
import CommentRepository from './CommentRepository';

export {default as ChapterRepository} from './ChapterRepository';
export {default as LessonRepository} from './LessonRepository';
export {default as UserRepository} from './UserRepository';
export {default as CourseRepository} from './CourseRepository';
export {default as CommentRepository} from './CommentRepository';

const container = new Container();
container.bind(Database).toSelf();
container.bind(LessonRepository).toSelf();
container.bind(ChapterRepository).toSelf();
container.bind(UserRepository).toSelf();
container.bind(CourseRepository).toSelf();
container.bind(CommentRepository).toSelf();

export const repositoryContainer = container;
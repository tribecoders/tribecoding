import { injectable } from "inversify";
import {Pool, QueryResult, PoolClient } from 'pg';
const database = require('../../database.json');

@injectable()
export default class Database {
  private pool: Pool | undefined;

  constructor() {
    const environment = process.env.NODE_ENV;
    if (environment) {
      this.pool = new Pool({
        host: database[environment].host,
        user: process.env.POSTGRES_USER,
        password: process.env.POSTGRES_PASSWORD,
        database: process.env.POSTGRES_DB,
        port: database[environment].port,
        connectionTimeoutMillis : 5000,
        idleTimeoutMillis : 30000
      });

      this.pool.on('error', (err) => {
        console.error('Unexpected error on DB client', err)
      });
    } else {
      console.error("Cannot connect to database");
    }
  }

  public async makeQuery(query: string, params: any[]) {
    return new Promise<QueryResult>((resolve, reject) => {
      if (!this.pool) {
          reject("No database connection");
      } else {
        this.pool.query(query, params, (err: Error, result: QueryResult) => {
          if(err) {
            console.error(err);
            reject(err);
          }
          resolve(result);
        })
      }
    });
  }

  public async makeTransactionQuery(client: PoolClient, query: string, params: any[]) {
    const result = await client.query(query, params);
    return result;
  }

  public async beginTransaction() {
    try {
      if (!this.pool) {
        throw new Error("No database connection");
      } else {
        const client = await this.pool.connect();
        client.query('BEGIN');
        return client;
      }
    } catch (err) {
      console.error(err);
      throw new Error('Begin transaction failed');
    }
  }

  public async commitTransaction(client: PoolClient) {
    try {
      await client.query('COMMIT');
    } catch (err) {
      console.error(err);
      throw new Error('Commiy transaction failed');
    }
    client.release();
  }

  public async rollbackTransaction(client: PoolClient) {
    try {
        await client.query('ROLLBACK');
    } catch (err) {
      console.error(err);
      throw new Error('Commiy transaction failed');
    }
    client.release();
  }
}
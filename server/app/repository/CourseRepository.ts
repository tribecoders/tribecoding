import Persisted from "./Persisted";
import { injectable } from "inversify";

@injectable()
export default class CourseRepository extends Persisted {

  public async enrollLesson(userId: string, lessonId: string, price: number) {
    const client = await this.database.beginTransaction();
    try {
      const enrollQuery = 'INSERT INTO user_lesson (user_id, lesson_id, price) VALUES ($1, $2, $3)';
      await this.database.makeTransactionQuery(client, enrollQuery, [userId, lessonId, price]);
      const deductCreditsQuery = 'UPDATE user_credits SET credits = credits - $2 WHERE user_id = $1';
      await this.database.makeTransactionQuery(client, deductCreditsQuery, [userId, price]);
      await this.database.commitTransaction(client);
    } catch(err) {
      console.error(err);
      this.database.rollbackTransaction(client);
      throw new Error('Enrolling lesson failed');
    }
  }

  public async selectEnrolledLessons(userId: string): Promise<string[]> {
    const query = 'SELECT lesson_id FROM user_lesson WHERE user_id = $1';
    const dbResult = await this.database.makeQuery(query, [userId]);
    const lessons = dbResult.rows.map(row => row['lesson_id']);
    return lessons;
  }

  public async isEnrolled(userId: string, lessonId: string): Promise<boolean> {
    const query = 'SELECT lesson_id FROM user_lesson WHERE user_id = $1 AND lesson_id = $2';
    const dbResult = await this.database.makeQuery(query, [userId, lessonId]);
    return dbResult.rows.length > 0;
  }

  public async updateUserChapter(userId: string, lessonId: string, chapterNo: number) {
    const query = 'INSERT INTO user_chapter (user_id, lesson_id, chapter_no) VALUES ($1, $2, $3) ON CONFLICT (user_id, lesson_id) DO UPDATE SET chapter_no = $3';
    await this.database.makeQuery(query, [userId, lessonId, chapterNo]);
  }

  public async selectUserChapter(userId: string, lessonId: string): Promise<number> {
    const query = 'SELECT chapter_no FROM user_chapter WHERE user_id = $1 AND lesson_id = $2';
    const dbResult = await this.database.makeQuery(query, [userId, lessonId]);
    return dbResult.rows.length > 0 ? parseInt(dbResult.rows[0]['chapter_no']) : 0;
  }
}
import { injectable } from "inversify";
import Persisted from "./Persisted";
import {User, UserCredits} from "../entity";
import bcrypt from 'bcrypt';

@injectable()
export default class UserRepository extends Persisted {
  async selectByEmailAndPassword(email: string, password: string) {
    const query = 'SELECT id, name, email, is_teacher, password FROM "user" WHERE email = $1';
    const dbResult = await this.database.makeQuery(query, [email]);
    const user = dbResult.rows.map(row => new User(row['id'], row['name'], row['email'], row['is_teacher']))[0];
    const passwordHash = dbResult.rows[0].password;
    const isPasswordCorrect = await this.verifyPassword(password, passwordHash);
    if (isPasswordCorrect) {
      return user;
    }
  }

  async selectUser(id: string) {
    const query = 'SELECT id, name, email, is_teacher FROM "user" WHERE id = $1';
    const dbResult = await this.database.makeQuery(query, [id]);
    const user = dbResult.rows.map(row => new User(row['id'], row['name'], row['email'], row['is_teacher']))[0];
    return user;
  }

  async selectUserByToken(token: string) {
    const query = 'SELECT id, name, email, is_teacher FROM "user" WHERE token = $1';
    const dbResult = await this.database.makeQuery(query, [token]);
    const user = dbResult.rows.map(row => new User(row['id'], row['name'], row['email'], row['is_teacher']))[0];
    return user;
  }

  async selectUserByEmail(email: string) {
    const query = 'SELECT id, name, email, is_teacher FROM "user" WHERE email = $1';
    const dbResult = await this.database.makeQuery(query, [email]);
    const user = dbResult.rows.map(row => new User(row['id'], row['name'], row['email'], row['is_teacher']))[0];
    return user;
  }

  async validateEmail(email: string) {
    const query = 'SELECT id, name, email FROM "user" WHERE email = $1';
    const dbResult = await this.database.makeQuery(query, [email]);
    const valid = dbResult.rows.length === 0
    return valid;
  }

  async insertUser(id: string, name: string, email: string, password: string) {
    const passworHash = await this.encryptPassword(password);
    const query = 'INSERT INTO "user"(id, name, email, password, teacher) VALUES($1, $2, $3, $4)';
    await this.database.makeQuery(query, [id, name, email, passworHash, true]);
  }

  async updateUserPassword(id: string, password: string) {
    console.log(id);
    const passworHash = await this.encryptPassword(password);
    const query = 'UPDATE "user" SET password = $2 WHERE id = $1';
    await this.database.makeQuery(query, [id, passworHash]);
  }

  private async encryptPassword(password: string) {
    const saltRounds = 10;
    return new Promise(resolve => {
      bcrypt.genSalt(saltRounds, (err, salt) => {
        bcrypt.hash(password, salt, (err, hash) => {
          return resolve(hash);
        });
      });
    });
  }

  private async verifyPassword(password: string, passwordHash: string) {
    return new Promise (resolve => {
      bcrypt.compare(password, passwordHash, (err, res) => {
        resolve(res);
      });
    });
  }

  async updateOrInsertUserToken(userId: string, token: string) {
    const query = 'INSERT INTO user_token (user_id, token) VALUES ($1, $2) ON CONFLICT (user_id) DO UPDATE SET token = $2';
    await this.database.makeQuery(query, [userId, token]);
  }

  async selectUserCredits(userId: string) {
    const query = 'SELECT credits FROM user_credits  WHERE user_id = $1';
    const dbResult = await this.database.makeQuery(query, [userId]);
    let credits = await dbResult.rows.map(row => new UserCredits(row['credits']))[0];
    return credits ? credits : {credits: 0};
  }
}
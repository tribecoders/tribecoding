import Persisted from "./Persisted";
import { injectable } from "inversify";
import Position from '../entity/Position';
import Comment from '../entity/Comment';
import CommentResponse from "../entity/CommentResponse";

@injectable()
export default class CommentRepository extends Persisted {

  public async insertCommment(id:string, position: Position, icon: string, userId: string, text: string) {
    const query = 'INSERT INTO comment(id, lesson_id, chapter_id, position, position_details, line, icon, user_id, text) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)';
    await this.database.makeQuery(query, [id, position.lessonId, position.chapterId, position.position, position.positionDetails, position.line, icon, userId, text]);
  }

  public async selectComment(commentId: string) {
  }

  public async selectComments(lessonId: string, chapterId: string) {
    const query = 'SELECT c.*, u.name as user_name FROM comment c JOIN "user" u on u.id = c.user_id WHERE lesson_id = $1 AND chapter_id = $2 ORDER BY created DESC';
    const dbResult = await this.database.makeQuery(query, [lessonId, chapterId]);
    const comments = dbResult.rows.map(row => new Comment(row));
    return comments;
  }

  public async selectUserComments(userId: string) {

  }

  public async selectLessonComments(lessonId: string) {
    
  }

  public async insertCommentResponse(id: string, commentId: string, userId: string, text: string) {
    const query = 'INSERT INTO comment_response(id, comment_id, user_id, text) VALUES($1, $2, $3, $4)';
    await this.database.makeQuery(query, [id, commentId, userId, text]);
  }

  public async selectCommentResponses(commentId: string) {
    console.error(commentId);
    const query = 'SELECT c.*, u.name as user_name FROM comment_response c JOIN "user" u on u.id = c.user_id WHERE comment_id = $1 ORDER BY created ASC';
    const dbResult = await this.database.makeQuery(query, [commentId]);
    const comments = dbResult.rows.map(row => new CommentResponse(row));
    return comments;
  }
}
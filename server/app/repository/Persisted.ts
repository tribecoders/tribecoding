import Database from "./Database";
import { inject, injectable } from "inversify";

@injectable()
export default class Persisted {
  @inject(Database) protected database!: Database;

  arrayToString(data: string[]): string | undefined {
    if (data && data.length > 0) {
      return data.join(',');
    }
  }
}
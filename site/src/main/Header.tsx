import { Grid, Button, Container } from "@material-ui/core";
import { UserAuth } from './user';
import React from 'react';
import styled from 'styled-components';
import { HeaderItem } from "./HeaderItem";
import { Link } from "react-router-dom";
import { useAuth } from "../lib/AuthContext";

export default function Header() {
  const {user} = useAuth();

  return (  
    <HeaderContainer>
      <Container>
        <Grid container spacing={3}>
          <Grid item sm={6}>
            <Link to="/">
            <LogoContainer>
              <img src="/images/logo.png" srcSet="/images/logo@2x.png 2x, /images/logo.png 1x" alt="back"/>
              <div>TribeCoding</div>
            </LogoContainer>
            </Link>
          </Grid>
          <Grid item sm={6}>
          <ActionContent>
            <HeaderItem horizonalPadding={60}>
              {user && user.isTeacher &&
                <Link to='/publish'>
                  <Button color="secondary">publish your code example</Button>
                </Link>
              }
            </HeaderItem>
            <UserAuth></UserAuth>
          </ActionContent>
          </Grid>
        </Grid>
      </Container>
    </HeaderContainer>
  )
}

const HeaderContainer = styled.div `
  padding-top: 10px;
`

const LogoContainer = styled.div `
  display: flex;
  line-height: 34px;
  font-size: 20px;
  font-weight: bold;
  color: #008E80;
  text-decoration
`

const ActionContent = styled.div `
  font-size: 20px;
  display: flex;
  justify-content: flex-end;
`
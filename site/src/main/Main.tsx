import React, { useState, useEffect } from 'react';
import { Container, Grid } from '@material-ui/core';
import styles from "./Main.module.scss";
import ReactGA from 'react-ga';
import { LessonApi, Lesson, CourseApi } from '../api';
import { LessonCard } from './lesson/LessonCard';
import { useAuth } from '../lib/AuthContext';

export default function Main() {
  const [lessons, setLessons] = useState<Lesson[]>([]);
  const [enrolledLessons, setEnrolledLessons] = useState<string[]>([]);
  const {user} = useAuth();

  useEffect(() => {
    ReactGA.pageview('/Main');
  },[]);

  useEffect(() => {
    LessonApi.getLessons().then(lessons => {
      setLessons(lessons);
    });
  },[]);

  useEffect(() => {
    if (user) {
      CourseApi.getEnrolledLessons(user.token).then(enrolledLessons => {
        if (enrolledLessons && enrolledLessons.length > 0) {
          setEnrolledLessons(enrolledLessons);
        }
      })
    } else {
      setEnrolledLessons([]);
    }
  },[user]);

  return (
    <Container fixed>
      <div className={styles.motto}>
        Developers tribe.<br />Learn from code examples.
      </div>

      <Grid container spacing={3}>
      { lessons.map((lesson: Lesson) =>
      <Grid key={lesson.id} item xs={12} md={4}>
        <LessonCard lesson={lesson} isEnrolled={enrolledLessons.includes(lesson.id)}/>
      </Grid>
      )}
      </Grid>
    </Container>
  )
}
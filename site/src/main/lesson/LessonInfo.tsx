import React, { useEffect, useState } from "react"
import { LessonApi, Lesson } from "../../api";
import Teacher from "../../api/Teacher";
import styled from "styled-components";
import Revision from "../../api/Revsion";

export interface LessonInfoProps {
  lesson: Lesson;
}

export const LessonInfo:React.FunctionComponent<LessonInfoProps> = ({lesson}) => {
  const [author, setAuthor] = useState();
  const [updated, setUpdated] = useState();

  useEffect(() => {
    let isSubscribed = true;
    LessonApi.getLessonAuthor(lesson.id).then((teacher: Teacher | undefined) => {
      if (teacher && isSubscribed) {
        setAuthor(teacher.name);
      }
    });
    return () => {isSubscribed = false};
  }, [lesson]);

  useEffect(() => {
    let isSubscribed = true;
    LessonApi.getLessonRevision(lesson.id).then((revision: Revision | undefined) => {
      if (revision && isSubscribed) {
        setUpdated(revision.created);
      }
    });
    return () => {isSubscribed = false};
  }, [lesson]);

  const Info = styled.span`
    display: flex;
    justify-content: space-between;
    font-size: 12px;
    flex-direction: column;
`

  const Container = styled.span`
  `

  const ContentBefore = styled.span`
    color: #AAAAAA;
  `

  return (
    <Info>
      <Container><ContentBefore>created by </ContentBefore>{author}</Container>
      <Container><ContentBefore>created on </ContentBefore>{lesson.created}</Container>
      <Container><ContentBefore>last update on </ContentBefore>{updated}</Container>
    </Info>
    
  )
}
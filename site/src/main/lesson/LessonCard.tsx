import { Card, CardMedia, CardContent, Typography, Dialog, DialogTitle, DialogContent, DialogActions, Button, useTheme, useMediaQuery, CardActions} from "@material-ui/core";
import { Lesson, Chapter, ChapterApi } from "../../api";
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Enroll } from "./Enroll";
import { LessonInfo } from "./LessonInfo";

export interface LessonProps {
  lesson: Lesson;
  isEnrolled: boolean;
}

export const LessonCard:React.FunctionComponent<LessonProps> = ({lesson, isEnrolled}) => {
  const [chapters, setChapters] = useState<Chapter[]>([]);
  const [enroll, setEnroll] = useState(false);
  const [scroll, setScroll] = useState(false);
  const [open, setOpen] = useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  useEffect(() => {
    ChapterApi.getLessonChapters(lesson.revision).then(chapters => {
      setChapters(chapters);
    })
  }, [lesson]);

  useEffect(() => {
    const scrollFunction = () => setScroll(true);
    window.addEventListener('scroll', scrollFunction);
    setTimeout(() => {
      setScroll(false);
    }, 500)
    return () => { window.removeEventListener('scroll', scrollFunction) }
  },[]);

  return (
    <LessonCardContainer>
      {enroll && <Enroll lessonSlug={lesson.slug} finishCallback={() => {setEnroll(false)}}></Enroll>}
      <Card>
        <Hover>
          <Message>
            <EnrollMessage onClick={() => setEnroll(true)} onTouchEnd={() => {
                if (!scroll ) {setEnroll(true)}
                }}>{isEnrolled ? <span>Explore</span> : <span>Explore</span>}</EnrollMessage>
          </Message>
          <CardMedia style={styles.lessonIcon}
              image={lesson.image}
              title={lesson.name} onClick={() => setEnroll(true)} onTouchEnd={() => {
                if (!scroll ) {setEnroll(true)}
                }}>
          </CardMedia>
        </Hover>
        {/*!isEnrolled && <Price value={lesson.price}/>*/}
        <CardContent style={{height: '250px'}}>
          <Typography gutterBottom variant="h5">
          {lesson.name}
          </Typography>
          <Typography gutterBottom variant="h6">
          {lesson.tags.join(', ')}
          </Typography>
          <Typography gutterBottom variant="body2" color="textSecondary" component={'span'}>
          {lesson.description}
          </Typography>
          <Typography style={{cursor: 'pointer', color: '#008E80'}} variant="body2" color="textSecondary" component={'span'} onClick={() => setOpen(true)} onTouchEnd={() => {
            if (!scroll ) {setOpen(true)}
          }}>
            &nbsp;more...
          </Typography>
        </CardContent>
        <CardActions onClick={() => setEnroll(true)} onTouchEnd={() => {
          if (!scroll ) {setEnroll(true)}
          }}>
          <Button style={{width: '100%', height:'50px'}} color="primary" variant="contained">{isEnrolled ? <span>Explore</span> : <span>Explore</span>}</Button>
        </CardActions>
      </Card>
      <Dialog open={open} onClose={() => setOpen(false)} aria-labelledby="form-dialog-title" maxWidth="sm" fullWidth fullScreen={fullScreen}>
            <DialogTitle id="form-dialog-title">
              {lesson.name}
              <CardMedia style={styles.lessonIcon}
              image={lesson.image}></CardMedia>
            </DialogTitle>
            <DialogContent>
            <Typography gutterBottom variant="body2" color="textSecondary" component={'span'}>
            {lesson.description}
            </Typography>
            <Typography variant="body2" color="textSecondary" component={'span'}>
            <ul>
            {chapters && chapters.length > 1 && chapters.map((chapter: Chapter, index: number) => 
                <li key={index}>
                  {chapter.name}
                </li>
              )}
            </ul>
            </Typography>
            <Typography gutterBottom variant="body2" color="textSecondary" component={'p'}>
              <a href={lesson.repository}>{lesson.repository}</a>
            </Typography>
            <Typography>
              <LessonInfo lesson={lesson} />
            </Typography>
            </DialogContent>
            <DialogActions>
              <Button variant="contained" color="primary" onClick={() => {setOpen(false);setEnroll(true)}} onTouchEnd={() => {setOpen(false);setEnroll(true);}}>
              {isEnrolled ? <span>Explore</span> : <span>Explore</span>}
              </Button>
              <Button onClick={() => setOpen(false)} variant="outlined" color="secondary">
                Cancel
              </Button>
            </DialogActions>
          </Dialog>
    </LessonCardContainer>
  );
}

const LessonCardContainer = styled.div `
    width: 100%;
    position: relative;
  `

  const styles = {
    lessonIcon: {
      height: '200px'
    },

    lessonAction: {
      justifyContent: 'space-between'
    }
  }

  const Message = styled.div`
    display: none;
  `

  const Hover = styled.div`
  cursor: pointer;
    &:hover ${Message} {
        display:block;
      }
  `
  const EnrollMessage = styled.h1`
      z-index: 1;
      position: absolute;
      top: 0;
      width: 100%;
      height: 200px;
      text-align: center;
      background-color: #348538;
      line-height: 200px;
      color: #000000;
      margin: 0;
  `;

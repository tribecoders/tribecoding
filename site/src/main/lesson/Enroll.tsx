import { Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Link } from "@material-ui/core";
import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { useAuth } from "../../lib/AuthContext";
import { useStatus } from "../../lib/StatusContext";
import { CourseApi, handleErrors, LessonApi } from "../../api";
import PubSub from 'pubsub-js'
import { MessageTopics } from '../../lib/MessageTopics';

export interface EnrollProps {
  lessonSlug: string;
  finishCallback: () => void
}

export const Enroll:React.FunctionComponent<EnrollProps> = ({lessonSlug, finishCallback}) => {
  const [infoOpen, setInfoOpen] = useState(false)
  const {user} = useAuth();
  const { setMessage } = useStatus();
  const history = useHistory();

  const closeMessage = () => {
    setInfoOpen(false);
    finishCallback();
  }

  const login = () => {
    closeMessage();
    PubSub.publish(MessageTopics.login, {});  
  }

  const register = () => {
    closeMessage()
    PubSub.publish(MessageTopics.login, {});  
  }

  useEffect(() => {
    if (user) {
      CourseApi.postLessonEnroll(lessonSlug, user.token).then(success => {
          if (success && history) {
            history.push(`/course/${lessonSlug}`);
          }
        }).catch((err) => {
          console.error(err);
          handleErrors(err.errors, setMessage);
          finishCallback();
        }); 
    } else {
      LessonApi.getLessonBySlug(lessonSlug).then(lesson => {
        if (lesson && !lesson.price && history) {
          history.push(`/course/${lessonSlug}`);
        } else {
          setInfoOpen(true);
        }
      });
    }
  },[history, user, lessonSlug, setMessage, finishCallback]);

  
  return (
    <Dialog onClose={closeMessage} aria-labelledby="simple-dialog-title" open={infoOpen}>
      <DialogTitle id="simple-dialog-title">Enroll with tribecoding</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Please <Link onClick={login} style={{cursor: 'pointer'}}>login</Link> or <Link onClick={register} style={{cursor: 'pointer'}}>register</Link> to start learning. Earn coins and enroll on all of our courses.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => {
          closeMessage()
        }} color="primary" autoFocus>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}
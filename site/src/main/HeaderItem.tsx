
import styled from 'styled-components';
import styledProps from 'styled-components-ts'
export interface HeaderItemProps {
  horizonalPadding: number;
}

export const HeaderItem = styledProps<HeaderItemProps>(styled.div) `
    padding-right: ${props => props.horizonalPadding}px;
  `
import React from "react";
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import styled from 'styled-components';

export interface PricePoprs {
  value: number;
}
export const Price:React.FunctionComponent<PricePoprs> = ({value}) => {
  return (
    <Container>
      {value && value > 0 ? <PriceValue><MonetizationOnIcon style={{color:'gold'}}/>{value}</PriceValue> : <img style={{width:'100px'}} alt="free" src='/images/free.png'/> }
    </Container>
  )
}

const Container = styled.div`
  position: absolute;
  right: 0;
  top:0;
  z-index: 2;
`

const PriceValue = styled.div `
  display: flex;
  align-items: center;
  color: gold;
  font-size: 16px;
  margin: 10px;
  padding: 10px;
  background-color: #008E80;
  border-radius: 20px;
  border: 1px solid gold;
`
import { Grid } from "@material-ui/core";
import React from "react";
import styled from 'styled-components';

export default function Footer() {
  return (
    <FooterContainer>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <Content>
            Contact us <a href="mailto:contact@tribecoding.com">contact@tribecoding.com</a><br />
            Developed by <a href="tribecoders.com">tribecoders.com</a>
          </Content>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Content>
            <a href="terms_and_conditions.html">Terms and conditions</a><br />
            <a href="cookies_policy.html">Cookies policy</a><br />
            <a href="privacy_policy.html">Privacy policy</a><br />
          </Content>
        </Grid>
      </Grid>
    </FooterContainer>
  )
}

const FooterContainer = styled.div `
  border-top: 1px solid #008E80;
  bottom: 10px;
  text-align: center;
  background-color: #fafafa;
  padding: 20px 12px 0 12px;
  margin-top: 20px;
`

const Content = styled.div `
  margin: auto;
  text-align: center;
`

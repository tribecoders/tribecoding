import React, { useState } from "react";
import { HeaderItem } from "../HeaderItem";
import { Button } from "@material-ui/core";
import { UserDrawer } from "./UserDrawer";
import { useAuth } from "../../lib/AuthContext";
import styled from 'styled-components';
// import { UserApi } from "../../api";
// import { Money } from "./Money";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default function UserAuthContent() {
  const [userOpen, setUserOpen] = useState(false);
  /*const [credits, setCredits] = useState(0);*/
  const { user } = useAuth();
  
  /*useEffect(() => {
    let canceled = false;
    if (user) {
      UserApi.getUserCredits(user.token).then(credits => {
        if (!canceled && credits) {
          setCredits(credits);
        }
      });
    }

    return () => {canceled = true;};
  }, [user]);*/

  const handleUserOpen = () => {
    setUserOpen(true);
  }

  const handleUserClose = () => {
    setUserOpen(false)
  }
  
  return (
    <Container>
      <HeaderItem  horizonalPadding={0}>
        <Button color="secondary" onClick={handleUserOpen}>
          {user!.name}<ExpandMoreIcon/>
        </Button>
      </HeaderItem>
      {/*<HeaderItem  horizonalPadding={0}>
          <Money value={credits}/>
      </HeaderItem>*/}
      <UserDrawer open={userOpen} onClose={handleUserClose}></UserDrawer>
    </Container>
  );
}

const Container = styled.div `
  display: flex;
`
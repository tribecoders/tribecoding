
import React from "react";
import { useAuth } from "../../lib/AuthContext";
import UserAuthContent from "./UserAuthContent";
import UserNotAuthContent from "./UserNothAuthContent";

export default function UserAuth() {
  const { user } = useAuth();
  return (
      <div>
      {user ? (
        <UserAuthContent/>
      ) : (
        <UserNotAuthContent/>
      )}
      </div>
  );
}
import styled from 'styled-components';
import { Button } from '@material-ui/core';
import { HeaderItem } from '../HeaderItem';
import { LoginDialog } from './LoginDialog';
import { RegisterDialog } from './RegisterDialog';
import React, { useState, useEffect } from 'react';
import PubSub from 'pubsub-js'
import { MessageTopics } from '../../lib/MessageTopics';
import { RequestPasswordDialog } from './RequestPasswordDialog';

export default function UserNotAuthContent() {
  const [loginOpen, setLoginOpen] = useState(false);
  const [registerOpen, setRegisterOpen] = useState(false);
  const [recoverOpen, setRecoverOpen] = useState(false);

  useEffect(() => {
    const loginSubscription = PubSub.subscribe(MessageTopics.login, handleLoginOpen);
    const registerSubscription = PubSub.subscribe(MessageTopics.register, handleRegisterOpen);

    return () => {
      PubSub.unsubscribe(loginSubscription);
      PubSub.unsubscribe(registerSubscription);
    }
  })

  const handleLoginOpen = () => {
    setRegisterOpen(false);
    setRecoverOpen(false);
    setLoginOpen(true);
  };

  const handleRegisterOpen = () => {
    setLoginOpen(false);
    setRecoverOpen(false);
    setRegisterOpen(true);
  };

  const handleSwitch = () => {
    setLoginOpen(!loginOpen);
    setRegisterOpen(!registerOpen);
  }

  const openRecover = () => {
    setLoginOpen(false);
    setRegisterOpen(false);
    setRecoverOpen(true);
  }

  return (
    <Container>
      <HeaderItem horizonalPadding={10}>
        <Button color="secondary" onClick={handleRegisterOpen}>
          Register
        </Button>
      </HeaderItem>
      <HeaderItem horizonalPadding={10}>
        <Button variant="outlined" color="primary" onClick={handleLoginOpen}>
          Login
        </Button>
      </HeaderItem>
      <LoginDialog open={loginOpen} handleClose={() => setLoginOpen(false)} handleSwitch={handleSwitch} openRecover={openRecover} />
      <RegisterDialog open={registerOpen} handleClose={() => setRegisterOpen(false)} handleSwitch={handleSwitch}/>
      <RequestPasswordDialog open={recoverOpen}  handleClose={() => setRecoverOpen(false)}/>
    </Container>
  )
}

const Container = styled.div `
  display: flex;
`
import { Button, Dialog, DialogContent, DialogTitle, DialogContentText, TextField, DialogActions, useMediaQuery, useTheme } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { UserApi, handleErrors } from "../../api";
import { useStatus } from "../../lib/StatusContext";
import ReactGA from 'react-ga';
import { useAuth } from "../../lib/AuthContext";

export interface RegisterDialogProps {
  open: boolean;
  handleClose: () => void;
  handleSwitch: () => void;
}

export const RegisterDialog:React.FunctionComponent<RegisterDialogProps> = ({open, handleClose, handleSwitch}) => {
  const { setUser } = useAuth();
  const { setMessage } = useStatus();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [errors, setErrors] = useState<Map<string, string>>(new Map());

  useEffect(() => {
    if (open) {
      ReactGA.pageview('/Register');
    }
  }, [open]);

  const handleSubmit = (event: any) => {
    event.preventDefault();
    const name = event.target.name.value;
    const email = event.target.email.value;
    const password = event.target.password.value;
    
    setErrors(new Map());
    UserApi.postUser(name, email, password).then((success: boolean) => {
      if (success) {
        UserApi.login(email, password).then((user) => {
          if (user) {
            setUser(user);
          }
        });
        setMessage('Registration successful', 'success');
        handleClose();
      }
    }).catch((err) => {
      console.error(err);
      handleErrors(err.errors, setMessage, setErrors);
    }); 
  }

  const validateEmail = (event: any) => {
    clearParamError(event);
    const email = event.target.value;
    UserApi.validateEmail(email).then(() => {
    }).catch((err) => {
      console.error(err);
      handleErrors(err.errors, setMessage, setErrors);
    });
  }

  const clearParamError = (event: any) => {
    const param = event.target.name;
    var newErrors = new Map(errors)
    newErrors.delete(param);
    setErrors(newErrors);
  }

  const SwitchLink = styled.a `
    cursor: pointer;
  `

  return (
    <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" fullScreen={fullScreen}>
      <form onSubmit={handleSubmit} noValidate>
        <DialogTitle id="form-dialog-title">Register</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Register to tribecoding.com and try out our courses.
          </DialogContentText>
          <TextField
              margin="dense"
              name="name"
              label="Name"
              type="text"
              error={errors.has("name")}
              helperText={errors.get("name")}
              autoComplete="name"
              fullWidth
              onBlur={clearParamError}
            />
            <TextField
              margin="dense"
              name="email"
              label="Email Address"
              type="email"
              error={errors.has("email")}
              helperText={errors.get("email")}
              autoComplete="email"
              fullWidth
              onBlur={validateEmail}
            />
            <TextField
              margin="dense"
              name="password"
              label="Password"
              type="password"
              error={errors.has("password")}
              helperText={errors.get("password")}
              autoComplete="password"
              fullWidth
              onBlur={clearParamError}
            />
          <DialogContentText>
            I have account already. Switch to <SwitchLink onClick={handleSwitch}>login</SwitchLink>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} variant="outlined" color="secondary">
            Cancel
          </Button>
          <Button type="submit" variant="outlined" color="primary" disabled={errors.size > 0 || false}>
            Register
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}
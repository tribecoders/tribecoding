import { Button, Dialog, DialogContent, DialogTitle, DialogContentText, TextField, DialogActions, useMediaQuery, useTheme } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useAuth } from "../../lib/AuthContext";
import { useStatus } from "../../lib/StatusContext";
import { UserApi, handleErrors } from "../../api";
import ReactGA from 'react-ga';

export interface LoginDialogProps {
  open: boolean;
  handleClose: () => void;
  handleSwitch: () => void;
  openRecover: () => void;
}

export const LoginDialog:React.FunctionComponent<LoginDialogProps> = ({open, handleClose, handleSwitch, openRecover}) => {
  const { setUser } = useAuth();
  const { setMessage } = useStatus();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [errors, setErrors] = useState<Map<string, string>>(new Map());

  useEffect(() => {
    if (open) {
      ReactGA.pageview('/Login');
    }
  }, [open]);

  const handleSubmit = (event: any) => {
    event.preventDefault();
    const email = event.target.email.value;
    const password = event.target.password.value;
    
    UserApi.login(email, password).then((user) => {
      if (user) {
        setUser(user);
        setMessage('Login successfull', 'success');
      }
    }).catch((err) => {
      console.error(err);
      handleErrors(err.errors, setMessage, setErrors);
    }); 
  }

  const clearParamError = (event: any) => {
    const param = event.target.name;
    var newErrors = new Map(errors)
    newErrors.delete(param);
    setErrors(newErrors);
  }

  const SwitchLink = styled.a `
    cursor: pointer;
  `

  return (
    <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" fullScreen={fullScreen}>
      <form onSubmit={handleSubmit} noValidate>
        <DialogTitle id="form-dialog-title">Login</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Welcome back to tribecoding.com.
          </DialogContentText>
          <TextField
            margin="dense"
            name="email"
            label="Email Address"
            type="email"
            autoComplete="email"
            error={errors.has("email")}
            helperText={errors.get("email")}
            fullWidth
            onBlur={clearParamError}
          />
          <TextField
            margin="dense"
            name="password"
            label="Password"
            type="password"
            autoComplete="password"
            error={errors.has("password")}
            helperText={errors.get("password")}
            fullWidth
            onBlur={clearParamError}
          />
          <DialogContentText>
            Create <SwitchLink onClick={handleSwitch}>new account</SwitchLink>.
          </DialogContentText>
          <DialogContentText>
            <SwitchLink onClick={openRecover}>Reset</SwitchLink> a password to account.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} variant="outlined" color="secondary">
            Cancel
          </Button>
          <Button type="submit" variant="outlined" color="primary">
            Login
          </Button>
        </DialogActions>
        </form>
      </Dialog>
  )
}
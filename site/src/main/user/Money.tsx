import React from "react";
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import styled from 'styled-components';
import { Tooltip } from "@material-ui/core";

export interface MoneyPoprs {
  value: number;
}
export const Money:React.FunctionComponent<MoneyPoprs> = ({value}) => {
  return (
    <Tooltip title="current credits">
      <Price>
          <MonetizationOnIcon style={{color:'gold'}}/>{value}
        
      </Price>
    </Tooltip>
  )
}

const Price = styled.div `
  display: flex;
  align-items: center;
  font-size: 16px;
  background-color: #008E80;
  border-radius: 20px;
  border: 1px solid gold;
  padding: 10px 20px;
  color: gold;
`
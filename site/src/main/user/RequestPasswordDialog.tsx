import { Button, Dialog, DialogContent, DialogTitle, DialogContentText, TextField, DialogActions, useMediaQuery, useTheme } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { useStatus } from "../../lib/StatusContext";
import { UserApi, handleErrors } from "../../api";
import ReactGA from 'react-ga';

export interface RequestPasswordDialogProps {
  open: boolean;
  handleClose: () => void;
}

export const RequestPasswordDialog:React.FunctionComponent<RequestPasswordDialogProps> = ({open, handleClose}) => {
  const { setMessage } = useStatus();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [errors, setErrors] = useState<Map<string, string>>(new Map());

  useEffect(() => {
    if (open) {
      ReactGA.pageview('/RequestPassword');
    }
  }, [open]);

  const handleSubmit = (event: any) => {
    event.preventDefault();
    const email = event.target.email.value;
    
    UserApi.requestPassword(email).then(success => {
      if (success) {
        setMessage('You will receive an email with an istruction to set new password.', 'success');
      }
    }).catch((err) => {
      console.error(err);
      handleErrors(err.errors, setMessage, setErrors);
    }); 
  }

  const clearParamError = (event: any) => {
    const param = event.target.name;
    var newErrors = new Map(errors)
    newErrors.delete(param);
    setErrors(newErrors);
  }

  return (
    <Dialog open={open} onClose={handleClose} maxWidth="sm" fullWidth={true} aria-labelledby="form-dialog-title" fullScreen={fullScreen}>
      <form onSubmit={handleSubmit} noValidate>
        <DialogTitle id="form-dialog-title">Reset password</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Reset password for
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            name="email"
            label="Email Address"
            type="email"
            autoComplete="email"
            error={errors.has("email")}
            helperText={errors.get("email")}
            fullWidth
            onBlur={clearParamError}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} variant="outlined" color="secondary">
            Cancel
          </Button>
          <Button type="submit" variant="outlined" color="primary">
            Reset
          </Button>
        </DialogActions>
        </form>
      </Dialog>
  )
}
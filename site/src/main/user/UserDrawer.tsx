import React from "react";
import { Drawer, List, ListItem, ListItemText, Divider, ListItemIcon } from "@material-ui/core";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
// import LockIcon from '@material-ui/icons/Lock';
// import AccountBoxIcon from '@material-ui/icons/AccountBox';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import { useAuth } from "../../lib/AuthContext";
import {Link} from 'react-router-dom';

export interface UserDrawerProps {
  open: boolean;
  onClose: () => void;
}

const listLinkStyle = {
  display: 'flex',
  color: 'inherit',
  textDecoration: 'none'
};

export const UserDrawer:React.FunctionComponent<UserDrawerProps> = ({open, onClose}) => {
  const { setUser } = useAuth();

  return (
    <Drawer anchor="right" open={open} onClose={onClose}>
      <List>
        {/*<ListItem button>
          <ListItemIcon><AccountBoxIcon/></ListItemIcon>
          <ListItemText>User details</ListItemText>
        </ListItem>*/}
        <ListItem button>
          <Link to='/user/code' style={listLinkStyle}>
            <ListItemIcon><LibraryBooksIcon/></ListItemIcon>
            <ListItemText>My code examples</ListItemText>
          </Link>
        </ListItem>
        {/*<ListItem button>
          <ListItemIcon><LockIcon/></ListItemIcon>
          <ListItemText>Change password</ListItemText>
        </ListItem>*/}
      </List>
      <Divider />
      <List>
        <ListItem button onClick={() => { setUser(undefined) }}>
          <ListItemIcon><ExitToAppIcon/></ListItemIcon>
          <ListItemText>Logout</ListItemText>
        </ListItem>
      </List>
    </Drawer>
  );
};

import React from 'react';
import Course from './course/Course';
export interface CourseProps {
  match:{
    params: {
      lessonSlug: string;
      chapterNo?: number;
      revision?: string;
    }
  }
}

export class CourseRoute extends React.Component<CourseProps, {}> {
  render() {
    return (
      <Course lessonSlug={this.props.match.params.lessonSlug} chapterNo={this.props.match.params.chapterNo} revision={this.props.match.params.revision}></Course>
    )
  }
}
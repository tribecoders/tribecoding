export enum MessageTopics {
  'login' = 'user_login',
  'register' = 'user_register',
}
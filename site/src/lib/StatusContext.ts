import { createContext, useContext } from 'react';
import { StatusVariant } from './StatusMessage';

export interface IStatusProps {
  setMessage: (message: string, variant: keyof typeof StatusVariant) => void;
}

export const StatusContext = createContext<IStatusProps>({setMessage: () => {}});

export function useStatus() {
  return useContext(StatusContext);
}
import { Snackbar } from "@material-ui/core"
import React from "react";
import { StatusMessage, StatusVariant } from "./StatusMessage";

export interface StatusProps {
  open: boolean;
  onClose?: () => void;
  message: string;
  variant: keyof typeof StatusVariant;
}

export const Status:React.FunctionComponent<StatusProps> = ({ open, onClose, message, variant }) => {

  return (
    <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={open}
        autoHideDuration={6000}
        onClose={onClose}
      >
        <StatusMessage message={message} variant={variant} onClose={onClose}></StatusMessage>
      </Snackbar>
  )
}
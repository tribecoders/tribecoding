import React from "react";
import { SnackbarContent, IconButton, makeStyles, Theme } from "@material-ui/core";
import CheckCircleIcon from '@material-ui/icons/Close';
import WarningIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import { green, amber } from "@material-ui/core/colors";
import clsx from 'clsx';

export const StatusVariant = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

export interface StatusMesssgeProps {
  message?: string;
  onClose?: () => void;
  variant: keyof typeof StatusVariant;
}

const useStyles = makeStyles((theme: Theme) => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.main,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
}));

export const StatusMessage:React.FunctionComponent<StatusMesssgeProps> = ({ message, onClose, variant, ...other }) => {
  const Icon = StatusVariant[variant];
  const classes = useStyles();

  return (
    <SnackbarContent
      className={clsx(classes[variant])}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={clsx(classes.icon, classes.iconVariant)}/>
          {message}
        </span>
      }
      action={[
        <IconButton key="close" aria-label="close" color="inherit" onClick={onClose}>
          <CloseIcon/>
        </IconButton>,
      ]}
      {...other}
    />
  );
}
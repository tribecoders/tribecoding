import { createContext, useContext } from 'react';
import User from '../api/User';

export interface IAuthProps {
  user: User | undefined;
  setUser: (user: User | undefined) => void;
}

export const AuthContext = createContext<IAuthProps>({user: undefined, setUser: () => {}});

export function useAuth() {
  return useContext(AuthContext);
}
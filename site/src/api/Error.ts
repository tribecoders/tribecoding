import { StatusVariant } from "../lib/StatusMessage";

export class ThrowableError {
  errors: ErrorMessage[];

  constructor(errors: ErrorMessage[] = []) {
    this.errors = errors;
  }
}

export interface ErrorMessage {
  loction?: string;
  msg: string;
  param?: string;
  value?: string;
}

export function handleErrors(err: ErrorMessage[], setMessage: (message: string, variant: keyof typeof StatusVariant) => void, setErrors: (errors: Map<string, string>) => void = () => {}) {
  const receivedErrors = new Map();
    err.forEach((err: ErrorMessage) => {
      if (!err.param) {
        setMessage(err.msg, 'error');
      } else {
        receivedErrors.set(err.param, err.msg);
      }
    });
    setErrors(receivedErrors);
}
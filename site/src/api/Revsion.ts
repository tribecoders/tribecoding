export default class Revision {
  revision: string;
  created: string;

  constructor(revision: string, created: string) {
    this.revision = revision;
    this.created = created;
  }

  static fromApiData(data: any) {
    return new Revision(data.revision, data.created);
  }
}
import Api from "./Api";
import User from "./User";

export default class UserApi extends Api {
  static async postUser(name: string, email: string, password: string) {
    try { 
      const params = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          'name': name,
          'email': email,
          'password': password
        })
      };
      return await this.fetch(`/api/user`, params);
    } catch(err) {
      this.throwError(err, 'Fail to register user!');
    }
  }

  static async login(email: string, password: string) {
    try { 
      const params = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          'email': email,
          'password': password
        })
      };
      const data = await this.fetch(`/api/user/login`, params);
      if (data.token && data.user && data.user.name) {
        return new User(data.token, data.user.name, data.user.isTeacher);
      }
    } catch (err) {
      this.throwError(err, 'Fail to login user!');
    }
  }

  static async getUserByToken(token: string) {
    try {
      const data = await this.fetch(`/api/user/token/${token}`);
      if (data && data.user) {
        return new User(token, data.user.name, data.user.isTeacher);
      }
    } catch (err) {
      this.throwError(err, 'Fail to get user by token!');
    }
  }

  static async validateEmail(email: string) {
    try {
      const params = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          'email': email
        })
      };
      const valid = await this.fetch(`/api/user/email`, params);
      return valid;
    } catch (err) {
      this.throwError(err, 'Email is already in use!');
    }
  }

  static async getUserCredits(token: string) {
    try {
      const params = {
        headers: { 'authorization': token }
      };
      const data = await this.fetch(`/api/user/credits`, params);
      if (data && data.credits) {
        return data.credits;
      }
    } catch (err) {
      console.error('Fail to get user credits!');
      return 0;
    }
  }

  static async requestPassword(email: string) {
    try { 
      const params = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          'email': email
        })
      };
      return await this.fetch(`/api/user/request`, params);
    } catch(err) {
      this.throwError(err, 'Fail to request password reset for user!');
    }
  }

  static async recoverPassword(timeBase64: string, userBase64: string, hash: string, password: string) {
    try { 
      const params = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          'timeBase64': timeBase64,
          'userBase64': userBase64,
          'hash': hash,
          'password': password,
        })
      };
      return await this.fetch(`/api/user/recover`, params);
    } catch(err) {
      this.throwError(err, 'Fail to recover password reset for user!');
    }
  }
}
import { ErrorMessage, ThrowableError } from "./Error";

export default class Api {
  static server = '';

  protected static async fetch(url: string, params?: RequestInit, asText = false,  errorMessage = 'Failed to communicate with tribecoding server!'): Promise<any | undefined> {
    try {
      const response =  params ? await fetch(`${Api.server}${url}`, params) : await fetch(`${Api.server}${url}`);
      if (response) {
        if (response.status === 401) {
          this.throwError();
        }
        const  data = asText ? await response.text() : await response.json();
        if(data && data.errors) {
          throw data.errors;
        }
        if (data && data !== 'false') {
          return data;
        } else {
          this.throwError();
        }
      }
    } catch (err) {
      this.throwError(err, errorMessage);
    }
  }

  // err can be
  // undefined
  // false
  // string
  // array of ErrorMessage
  // ThrowableError
  protected static throwError(err: undefined | boolean | string | ErrorMessage[] | ThrowableError = undefined, message: string | undefined = undefined): void {
      if (err instanceof ThrowableError) {
        if (message && err.errors.length === 0) {
          err = new ThrowableError([{msg: message}]);
        }
        throw err;
      }
      if (Array.isArray(err) && err.length > 0) {
        throw new ThrowableError(err);
      } else {
        if (message) {
          throw new ThrowableError([{msg: message}])
        } else {
          throw new ThrowableError();
        }
      }
  }
}
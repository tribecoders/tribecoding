export default class Position {
  lessonId: string
  chapterId: string
  position: string
  positionDetails: string
  line: number
  static LINE_UNKNOWN = -1;

  constructor(lessonId: string, chapterId: string, position: string, positionDetails: string, line = Position.LINE_UNKNOWN) {
    this.lessonId = lessonId;
    this.chapterId = chapterId;
    this.position = position;
    this.positionDetails = positionDetails;
    this.line = line;
  }

  static fromApiData(data: any) {
    return new Position(data.position.lessonId, data.position.chapterId, data.position.position, data.position.positionDetails, data.position.line);
  }

  toRawObject() {
    return {
      lessonId: this.lessonId,
      chapterId: this.chapterId,
      position: this.position,
      positionDetails: this.positionDetails,
      line: this.line
    }
  }
}
import Api from "./Api";
import Comment from './Comment';
import Position from './Position';
import CommentResponse from "./CommentResponse";

export default class CommentApi extends Api {
  static async postComment(position: Position, icon: string, text: string, token: string) {
    try {
      const params = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'authorization': token },
        body: JSON.stringify({
          position: position.toRawObject(),
          icon,
          text
        })
      };
      const success: boolean = await this.fetch(`/api/comment`, params);
      return success;
    } catch {
      console.error('Fail to add new comment!');
    }
  }

  static async getComments(lessonId: string, chapterId: string): Promise<Comment[]> {
    try {
      const data = await this.fetch(`/api/comment/lesson/${lessonId}/chapter/${chapterId}`);
      const comments: Comment[] = data.map((commentData:any) => Comment.fromApiData(commentData))
      return comments;
    } catch {
      console.error('Failed to load lesson comments!');
      return [];
    }
  }

  static async postCommentResponse(commentId: string, text: string, token: string) {
    try {
      const params = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'authorization': token },
        body: JSON.stringify({
          text
        })
      };
      const success: boolean = await this.fetch(`/api/comment/${commentId}/response`, params);
      return success;
    } catch {
      console.error('Fail to add new comment response!');
    }
  }

  static async getCommentResponses(commentId: string): Promise<CommentResponse[]> {
    try {
      const data = await this.fetch(`/api/comment/${commentId}/response`);
      const comments: CommentResponse[] = data.map((commentData:any) => CommentResponse.fromApiData(commentData))
      return comments;
    } catch {
      console.error('Failed to load comment responses!');
      return [];
    }
  }
}
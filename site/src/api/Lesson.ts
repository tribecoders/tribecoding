import { Chapter } from ".";

export default class Lesson {
  id: string;
  name: string;
  description: string;
  slug: string;
  image: string;
  tags: string[];
  repository: string;
  price: number;
  revision: string;
  created: string;
  chapters?: Chapter[];

  constructor(id: string, name: string, description: string, slug: string, image: string, tags: string[], repository: string, price: number, revision: string, created: string) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.slug = slug;
    this.image = image;
    this.tags = tags;
    this.repository = repository;
    this.price = price
    this.revision = revision;
    this.created = created;
  }

  static fromApiData(data: any) {
    return new Lesson(data.id, data.name, data.description, data.slug, data.image, data.tags, data.repository, parseInt(data.price), data.revision, data.created);
  }
}
import Position from './Position'
export default class Comment {
  id?: string;
  position: Position;
  icon: string;
  userId: string;
  userName?: string;
  text: string;

  constructor(position: Position, icon: string, userId: string, text: string, id?: string, userName?:string) {
    this.position = position;
    this.icon = icon;
    this.userId = userId;
    this.text = text;
    this.id = id;
    this.userName = userName;
  }

  static fromApiData(data: any) {
    const position = Position.fromApiData(data);
    return new Comment(position, data.icon, data.userId, data.text, data.id, data.userName);
  }

  toRawObject() {
    return {
      position: this.position.toRawObject(),
      icon: this.icon,
      text: this.text,
    }
  }
}
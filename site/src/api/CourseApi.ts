import Api from "./Api";

export default class CourseApi extends Api{

  static async postLessonEnroll(lessonSlug: string, token: string): Promise<undefined> {
    try {
      const params = {
        method: 'POST',
        headers: { 
          'Content-Type': 'application/json',
          'authorization': token 
        }
      };
      const data = await this.fetch(`/api/lesson/slug/${lessonSlug}/enroll`, params);
      return data;
    } catch(err) {
      this.throwError(err, 'Fail to enroll lesson!');
    }
  }
  
  static async getEnrolledLessons(token: string): Promise<string[]> {
    try {
      const params = {
        headers: { 'authorization': token }
      };
      const data = await this.fetch(`/api/user/lesson`, params);
      if (data) {
        return data;
      }
      return [];
    } catch {
      console.error('Failed to load enrolled lessons!');
      return [];
    }
  }

  static async postUserChapter(token: string, lessonId: string, chapterNo: number): Promise<boolean> {
    try {
      const params = {
        method: 'POST',
        headers: { 'authorization': token, 'Content-Type': 'application/json' },
        body: JSON.stringify({
          'chapterNo': chapterNo
        })
      };
      const success = await this.fetch(`/api/user/lesson/${lessonId}/chapter`, params);

      if (success && success !== "false") {
        return true;
      }
      return false;
    } catch {
      console.error('Failed to save last chapter!');
      return false;
    }
  }

  static async getUserChapter(token: string, lessonId: string): Promise<number> {
    try {
      const params = {
        headers: { 'authorization': token }
      };
      const data = await this.fetch(`/api/user/lesson/${lessonId}/chapter`, params);
      if (data && data['chapterNo']) {
        return parseInt(data['chapterNo']);
      }
      return 0;
    } catch {
      console.error('Failed to load last chapter!');
      return 0;
    }
  }

} 
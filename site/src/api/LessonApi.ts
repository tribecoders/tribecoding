import Api from './Api';
import Lesson from './Lesson';
import Teacher from './Teacher';
import Revision from './Revsion';

export default class LessonApi extends Api {

  static async postLesson(repository: string, token: string): Promise<{lessonSlug:string, revision:string} | undefined> {
    try {
      const params = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'authorization': token },
        body: JSON.stringify({
          'repository': repository
        })
      };
      const data = await this.fetch(`/api/lesson`, params);
      if (data && data.lessonSlug && data.revision) {
        return {lessonSlug: data.lessonSlug, revision: data.revision};
      }
    } catch {
      console.error('Fail to add new lesson!');
    }
  }

  static async getLessons(): Promise<Lesson[]> {
    try {
      const data = await this.fetch(`/api/lesson/`);
      let lessons: Lesson[] = data.map((chapterData:any) => Lesson.fromApiData(chapterData));
      lessons = lessons.sort((l1, l2) => Date.parse(l2.created) - Date.parse(l1.created));
      return lessons;
    } catch {
      console.error('Failed to load lesson chapters!');
      return [];
    }
  }

  static async getLesson(lessonId: string): Promise<Lesson | undefined> {
    try {
      const data = await this.fetch(`/api/lesson/${lessonId}`);
      const lesson = Lesson.fromApiData(data);
      return lesson;
    } catch {
      console.error('Failed to load lesson!');
    }
  }

  static async getLessonBySlug(slug: string): Promise<Lesson | undefined> {
    try {
      const data = await this.fetch(`/api/lesson/slug/${slug}`);
      const lesson = Lesson.fromApiData(data);
      return lesson;
    } catch {
      console.error('Failed to load lesson by slug!');
    }
  }

  static async getLessonAuthor(lessonId: string): Promise<Teacher | undefined> {
    try {
      const data = await this.fetch(`/api/lesson/${lessonId}/author`);
      const teacher = Teacher.fromApiData(data);
      return teacher;
    } catch {
      console.error('Failed to load lesson teacher');
    }
  }

  static async getLessonRevision(lessonId: string): Promise<Revision | undefined> {
    try {
      const data = await this.fetch(`/api/lesson/${lessonId}/revision`);
      const revision = Revision.fromApiData(data);
      return revision;
    } catch {
      console.error('Failed to load lesson revision');
    }
  }

  static async putLessonRevision(lessonId: string, revision: string, token: string) {
    try {
      const params = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json', 'authorization': token },
        body: JSON.stringify({
          'revision': revision
        })
      };
      await this.fetch(`/api/lesson/${lessonId}/revision`, params);
    } catch {
      console.error('Failed to update lesson revision');
    }
  }
} 
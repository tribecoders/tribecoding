import Api from "./Api";

export default class FileApi extends Api {
  static async getLessonFiles(revision: string, chapterId: string): Promise<string[]> {
    try {
      const data = await this.fetch(`/api/lessonRevision/${revision}/chapter/${chapterId}/file`);
      if (data && data !== "false") {
        return data;
      }
    } catch {
      console.error('Failed to load lesson files!');
    }
    return [];
  }
  
  static async getLessonFile(revision: string, chapterId: string, file: string, token?: string): Promise<string | undefined> {
    try {
      let params = {};
      if (token) {
        params = {
          headers: { 'authorization': token }
        }
      }
      const data = await this.fetch(`/api/lessonRevision/${revision}/chapter/${chapterId}/file/${file}`, params, true);
      if (data) {
        return data;
      }
    } catch {
      console.error('Failed to load lesson file!');
    }
  }
}
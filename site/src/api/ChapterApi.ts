import Api from "./Api";
import Chapter from './Chapter';

export default class ChapterApi extends Api {
  static async getLessonChapters(revision: string): Promise<Chapter[]> {
    try {
      const data = await this.fetch(`/api/lessonRevision/${revision}/chapter`);
      const chapters: Chapter[] = data.map((chapterData:any) => Chapter.fromApiData(chapterData))
      return chapters;
    } catch {
      console.error('Failed to load lesson chapters!');
      return [];
    }
  }

  static async getLessonChapterContent(revision: string, chapterId: string, token?: string): Promise<string | undefined> {
    try {
      let params = {};
      if (token) {
        params = {
          headers: { 'authorization': token }
        }
      }
      const data = await this.fetch(`/api/lessonRevision/${revision}/chapter/${chapterId}/content`, params, true);
      if (data && data !== "false") {
          return data;
      }
    } catch {
      console.error('Failed to load lesson chapter content!');
    }
  }
}
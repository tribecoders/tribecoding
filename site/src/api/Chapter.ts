export default class Chapter {
  id: string;
  name: string;
  commit: string;

  constructor(id: string, name: string, commit: string) {
    this.id = id;
    this.name = name;
    this.commit = commit;
  }

  static fromApiData(data: any) {
    return new Chapter(data.id, data.name, data.commit);
  }
}
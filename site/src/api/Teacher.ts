export default class Teacher {
  id: string;
  name: string;

  constructor(id: string, name: string, commit: string) {
    this.id = id;
    this.name = name;
  }

  static fromApiData(data: any) {
    return new Teacher(data.id, data.name, data.commit);
  }
}
export default class User {
  token: string;
  name: string;
  isTeacher: boolean;

  constructor(token: string, name: string, isTeacher: boolean = false){
    this.token = token;
    this.name = name;
    this.isTeacher = isTeacher;
  }
} 
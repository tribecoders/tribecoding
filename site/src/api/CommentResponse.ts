export default class CommentResponse {
  id?: string;
  userId: string;
  userName?: string;
  text: string;

  constructor(userId: string, text: string, id?: string, userName?:string) {
    this.userId = userId;
    this.text = text;
    this.id = id;
    this.userName = userName;
  }

  static fromApiData(data: any) {
    return new CommentResponse(data.userId, data.text, data.id, data.userName);
  }

  toRawObject() {
    return {
      text: this.text
    }
  }
}
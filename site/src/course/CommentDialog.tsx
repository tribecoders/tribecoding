import React, { useState, useEffect, useCallback } from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContent, Button, useTheme, useMediaQuery, TextareaAutosize } from "@material-ui/core"
import Comment from '../api/Comment';
import styled from 'styled-components';
import { CommentIcon } from './presentation/CommentIcon';
import CommentApi from '../api/CommentApi';
import { useAuth } from '../lib/AuthContext';
import CommentResponse from '../api/CommentResponse';
export interface CommentDialogProps {
  open: boolean
  onClose: () => void
  comment: Comment;
}

export const CommentDialog:React.FunctionComponent<CommentDialogProps> = ({open, onClose, comment}) => {
  const [respond, setRespond] = useState(false);
  const [responses, setResponses] = useState<CommentResponse[]>([])
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const { user } = useAuth();
  const loadResponses = useCallback(() => {
    if (comment && comment.id) {
      CommentApi.getCommentResponses(comment.id).then((commentResponses: CommentResponse[]) => {
        if (commentResponses) {
          setResponses(commentResponses);
        }
      })
    }
  },[comment])

  useEffect(() => {
    loadResponses();
  }, [loadResponses]);

  const addCommentResponse =  (event: any) => {
    event.preventDefault();
    const text = event.target.text.value;

    if (text && user && comment.id) {
      CommentApi.postCommentResponse(comment.id, text, user.token).then(success => { 
        if (success) {
          setRespond(false);
          loadResponses();
        }
      });
    }
  }

  return (
  <Dialog open={open} onClose={onClose} aria-labelledby="form-dialog-title" maxWidth="xs" fullWidth fullScreen={fullScreen}>
    <form onSubmit={addCommentResponse} noValidate>
      <DialogTitle id="form-dialog-title">
        { comment && <CommentIcon icon={comment.icon} /> }
      </DialogTitle>
      <DialogContent>
        { comment &&
          <CommentDetails>
            <Author>{comment.userName}</Author>
            {comment.text}
          </CommentDetails>
        }
        { responses.map(response => <span key={response.id}>
          {response.userId === comment.userId ? <Author>{response.userName}</Author> : <Response>{response.userName}</Response>}
          {response.text}
        </span>)}
        {respond && <TextareaAutosize aria-label="minimum height" name="text" rows={5} placeholder="comment..." style={{width: '100%', fontSize:'16px'}}/>}
      </DialogContent>
      <DialogActions>
        {user && <span>{respond ?
          <Button type="submit" variant="outlined" color="primary">Add</Button>
            :
          <Button onClick={() => setRespond(true)} type="button" variant="outlined" color="primary">Respond</Button>
        }</span>}
        <Button onClick={onClose} variant="outlined" color="secondary">
          Close
        </Button>
      </DialogActions>
    </form>
  </Dialog>
  )
}

const CommentDetails = styled.div `
  text-align: left;
`

const Author = styled.div `
  font-size: 10px;
  color: green;
`
const Response = styled.div `
  font-size: 10px;
  color: darkgoldenrod;
`
import React, { useState, useEffect, useCallback } from 'react';
import styles from './Page.module.scss'
import ReactMarkdown from 'react-markdown'
import Error from '../../lib/Error'
import { ChapterApi } from '../../api';
import { useAuth } from '../../lib/AuthContext';
import {Demo} from './Demo';
import {Video} from './Video';
import styled from 'styled-components';
import { Code } from '../Code';
import { AddComment } from '../AddComment';
import { CommentIndicator } from '../CommentIndicator';
import Position from '../../api/Position';
import CommentApi from '../../api/CommentApi';
import Comment from '../../api/Comment';

export interface PageProps {
  lessonId: string;
  chapterId: string;
}

export const Page:React.FunctionComponent<PageProps> = ({lessonId, chapterId, children}) => {
  const [content, setContent] = useState("");
  const [loading, setLoading] = useState(true);
  const [comments, setComments] = useState<Comment[][]>([])
  const {user} = useAuth();

  useEffect(() => {
    ChapterApi.getLessonChapterContent(lessonId, chapterId, user ? user.token : undefined).then(content => {
      if (content) {
        setContent(content);
      }
    }).finally(() =>{
      setLoading(false);
    });
  }, [lessonId, chapterId, user]);

  const loadComments = useCallback(
    () => {
      CommentApi.getComments(lessonId, chapterId).then(comments => {
        const pageComments: Comment[][] = [];
        comments.forEach(comment => {
          if (comment.position.position === 'text' || comment.position.position === 'inline') {
            const line = comment.position.line;
            if (!pageComments[line]) {
              pageComments[line] = [];
            }
            pageComments[line].push(comment);
          }
        });
        setComments(pageComments);
      })
    }, [lessonId, chapterId]
  );

  useEffect(() => {
    loadComments();
  }, [lessonId, chapterId, loadComments]);

  let lineNumber = 0;
  const renderers = { 
    inlineCode: (code: {value: string}) => {
      if (code.value.startsWith('https://www.youtube.com/embed')) {
        return (<Video url={code.value} />)
      }
      if (code.value.startsWith('http')) {
        return (<Demo url={code.value} />)
      }
      return (<InlineCode>{code.value}</InlineCode>)
    }, paragraph: (paragraph: any) => {
    return (
      <CommentOverlay>
        {comments[lineNumber] &&
          <CommentIndicator comments={comments[lineNumber]}></CommentIndicator>
        } 
        <AddComment onSuccess={loadComments} position={new Position(
          lessonId,
          chapterId,
          "text",
          "",
          lineNumber++
        )}>
          {paragraph.children.map((element: any) => element)}
        </AddComment>
      </CommentOverlay>);
    }, code: (code: {value: string}) => {
      return (
        <CommentOverlay>
          {comments[lineNumber] &&
            <CommentIndicator comments={comments[lineNumber]}></CommentIndicator>
          }
          <AddComment onSuccess={loadComments} position={new Position(
            lessonId,
            chapterId,
            "inline",
            "",
            lineNumber++
          )}>
            <Code code={code.value}/>
          </AddComment>
        </CommentOverlay>
      )
    } 
}

  return (
    <div className={styles.container}>
      {!loading && (
        <div>
          {content ?
            <div>
              <ReactMarkdown skipHtml={false} source={content} renderers={renderers}/>
              {children}
            </div>
            :
            <Error message="Unfortunatelly this chapter is unavailable"></Error>
          }
        </div>
      )}
    </div>
  )
}

const InlineCode = styled.span `
  color: #ffffff;
  background-color: #000000;
  line-height: 30px;
  display: inline-block;
  padding: 0 5px;
`

const CommentOverlay = styled.p`
  display: inline-block;
  width: 100%;
  &:hover {
    background-color: #dff3ef;
    cursor: cell;
  }
`;

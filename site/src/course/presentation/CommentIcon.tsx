import React from 'react';
import ErrorIcon from '@material-ui/icons/Error';
import WarningIcon from '@material-ui/icons/Warning';
import InfoIcon from '@material-ui/icons/InfoOutlined';

export interface CommentIconProps {
  icon: string
}

export const CommentIcon:React.FunctionComponent<CommentIconProps> = ({icon}) => {
  return (
    <span>
      {icon==='error' && <ErrorIcon style={{color: 'red', fontSize: '20px'}}></ErrorIcon>}
      {icon==='warning' &&<WarningIcon style={{color: 'orange', fontSize: '20px'}}></WarningIcon>}
      {icon==='info' &&<InfoIcon style={{color: 'blue', fontSize: '20px'}}></InfoIcon>}
    </span>
  )
}
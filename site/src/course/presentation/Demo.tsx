import React, { useState } from 'react';
import styled from 'styled-components';
import { Button, Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactGA from 'react-ga';

export interface DemoProps {
  url: string
}

export const Demo:React.FunctionComponent<DemoProps> = ({url}) => {
  const [isOpen, setOpen] = useState(false);

  const openDemo = () => {
    ReactGA.pageview(`/demo -> ${url}`);
    setOpen(true);
  }

  return  (
    <span>
      <Button onClick={openDemo} variant="outlined" color="primary">
        Open demo
      </Button>
      <Dialog open={isOpen} fullScreen={true}>
        <DialogContent style={{position:'relative'}}>
          <EmbededDemo id="demo" title='embeded demo' src={url}/>
        </DialogContent> 
        <DialogActions>
        
        <Button variant="outlined" color="secondary" onClick={()=> {
            const demoIframe:any = document.getElementById("demo"); 
            if (demoIframe) {
              demoIframe.parentNode.replaceChild(demoIframe.cloneNode(), demoIframe);
            }
          }}>Refresh demo
        </Button>
        <Button onClick={() => setOpen(false)} variant="outlined" color="primary">
          Close
        </Button>
        </DialogActions>   
      </Dialog>
    </span>
  )
}
const EmbededDemo = styled.iframe `
  border:none;
  width: 100%;
  height: 100%;
  padding: 0;
  position: absolute;
  -webkit-overflow-scrolling: touch;
`
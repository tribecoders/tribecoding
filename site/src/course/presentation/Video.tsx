import React from 'react';
import styled from 'styled-components';


export interface VideoProps {
  url: string
}

export const Video:React.FunctionComponent<VideoProps> = ({url}) => {

  return  (
    <VideoContainer> 
      <EmbededVideo width="560" height="315" src={url} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen={true}>
      </EmbededVideo>
    </VideoContainer>
  )
}
const VideoContainer = styled.div `
  position: relative; 
  padding-bottom: 56.25%; /* 16:9 */ 
  height: 0;
`;
const EmbededVideo = styled.iframe `
  position: absolute; 
  top: 0; 
  left: 0; 
  width: 100%; 
  height: 100%;
`
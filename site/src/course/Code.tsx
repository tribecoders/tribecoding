
import React from 'react';
import Highlight, { defaultProps } from 'prism-react-renderer'
import theme from 'prism-react-renderer/themes/nightOwl'
import styled from 'styled-components';
import { AddComment } from './AddComment';
import Position from '../api/Position';
import Comment from '../api/Comment';
import { CommentIndicator } from './CommentIndicator';

export interface CodeProps {
  code: string;
  position?: Position;
  comments?: Comment[][];
  loadComments?: () => void
}

export const Code:React.FunctionComponent<CodeProps> = ({code, position, comments, loadComments}) => {
  return (
    
    <Highlight {...defaultProps} theme={theme} code={code} language="jsx">
        {({ className, style, tokens, getLineProps, getTokenProps }) => (
          <Pre className={className} style={style}>
            {tokens.map((line, lineNumber) => (
              <Line {...getLineProps({ line, key: lineNumber })}>
                {comments && comments[lineNumber] &&
                  <CommentIndicator comments={comments[lineNumber]}></CommentIndicator>
                }
                { position ?
                <CommentOverlay>
                    <AddComment onSuccess={() => {
                      if (loadComments) {
                        loadComments();
                      }
                    }} position={new Position(
                        position.lessonId,
                        position.chapterId,
                        position.position,
                        position.positionDetails,
                        lineNumber
                      )}>
                    <LineNo>{lineNumber + 1}</LineNo>
                    {line.map((token, key) => <span {...getTokenProps({ token, key })} />)}
                  </AddComment>
                </CommentOverlay>
                :
                <span>
                  <LineNo>{lineNumber + 1}</LineNo>
                  {line.map((token, key) => <span {...getTokenProps({ token, key })} />)}
                </span>}
              </Line>
            ))}
          </Pre>
        )}
      </Highlight>
  )
}

const CommentOverlay = styled.span`
  display: inline-block;
  width: 100%;
  &:hover {
    background-color: #008E80;
    cursor: cell;
  }
`;

const Pre = styled.span`
  display: block;
  unicode-bidi: embed;
  font-family: monospace;
  white-space: pre;
  text-align: left;
  margin: 1em 0;
  padding: 0.5em;

  & .token-line {
    line-height: 1.3em;
    height: 1.3em;
  }
`

const Line = styled.span`
  display: block;
`;

const LineNo = styled.span`
  display: inline-block;
  width: 2em;
  user-select: none;
  opacity: 0.3;
`
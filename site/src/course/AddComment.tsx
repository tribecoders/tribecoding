import React, { useState } from 'react';
import CommentApi from '../api/CommentApi';
import Position from '../api/Position';
import { useAuth } from '../lib/AuthContext';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, useTheme, useMediaQuery, FormControlLabel, Radio, RadioGroup, TextareaAutosize, DialogContentText } from '@material-ui/core';
import ErrorIcon from '@material-ui/icons/Error';
import WarningIcon from '@material-ui/icons/Warning';
import InfoIcon from '@material-ui/icons/InfoOutlined';

export interface AddCommentProps {
  position: Position;
  onSuccess: () => void
}

export const AddComment:React.FunctionComponent<AddCommentProps> = ({position, onSuccess, children}) => {
  const [open, setOpen]= useState(false);
  const [registerOpen, setRegisterOpen] = useState(false);
  const [icon, setIcon] = useState("error");
  const { user } = useAuth();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const addComment =  (event: any) => {
    event.preventDefault();
    const text = event.target.text.value;

    if (user) {
      CommentApi.postComment(position, icon, text, user.token).then(success => { 
        if (success) {
          onSuccess();
          setOpen(false);
        }
      });
    }
  }

  const selectIcon = (event: React.ChangeEvent<HTMLInputElement>) => {
    setIcon((event.target as HTMLInputElement).value);
  };

  return (
    <span>
      {user ?
        <Dialog open={open} onClose={() => setOpen(false)} aria-labelledby="form-dialog-title" maxWidth="sm" fullWidth fullScreen={fullScreen}>
          <form onSubmit={addComment} noValidate>
            <DialogTitle id="form-dialog-title">Add a comment</DialogTitle>
            <DialogContent>
              <RadioGroup aria-label="gender" name="icon" value={icon} onChange={selectIcon} style={{flexDirection: 'row'}}>
                <FormControlLabel name="icon" value="error" control={<Radio />} label={<ErrorIcon style={{color: 'red'}}/>}></FormControlLabel>
                <FormControlLabel name="icon" value="warning" control={<Radio />} label={<WarningIcon style={{color: 'orange'}}/>}></FormControlLabel>
                <FormControlLabel name="icon" value="info" control={<Radio />} label={<InfoIcon style={{color: 'blue'}}/>}></FormControlLabel>
              </RadioGroup>
              <TextareaAutosize aria-label="minimum height" name="text" rows={5} placeholder="comment..." style={{width: '100%', fontSize:'16px'}}/>
            </DialogContent>
            <DialogActions>
              <Button type="submit" variant="outlined" color="primary">
                Add
              </Button>
              <Button onClick={() => setOpen(false)} variant="outlined" color="secondary">
                Cancel
              </Button>
            </DialogActions>
            </form>
          </Dialog>
          :
          <Dialog onClose={() => setRegisterOpen(false)} aria-labelledby="simple-dialog-title" open={registerOpen}>
            <DialogTitle id="simple-dialog-title">Add a comment</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Please login or register to post a comment. 
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={() => setRegisterOpen(false)} color="primary" autoFocus>
                Close
              </Button>
            </DialogActions>
          </Dialog>
      }
      <span onClick={() => user ? setOpen(true): setRegisterOpen(true)}>
        {children}
      </span>
    </span>
  )
}
import React from 'react';
import { Tabs, Tab} from '@material-ui/core';

export interface ChaptersProps {
  chapters: string[];
  chapterNo: number;
  selectChapter: (chapter: number) => void;
}

export interface ChaptersState {
  chaptersEnabled: number;
}

export default class Chapters extends React.Component<ChaptersProps, ChaptersState> {
  state = {
    chaptersEnabled: 0
  };

  componentWillReceiveProps(newProps: ChaptersProps) {
    if (newProps.chapterNo > this.state.chaptersEnabled) {
      this.setState({
        chaptersEnabled: newProps.chapterNo
      })
    }
  }

  selectChapter(chapter: number) {
    this.props.selectChapter(chapter);
  }

  handleChange(event: React.ChangeEvent<{}>, newValue: number) {
    this.selectChapter(newValue);
  };

  render() {
    return (
      <Tabs
        value={this.props.chapterNo+""}
        onChange={this.handleChange.bind(this)}
        indicatorColor="primary"
        textColor="primary"
        variant="scrollable"
        scrollButtons="auto"
      >
        {
          this.props.chapters.map((chapter, index: number) =>
            <Tab label={chapter} value={index+""} key={index}>{/*disabled={index > this.state.chaptersEnabled}*/}</Tab>
          )
        }
      </Tabs>
      )
  }
}

// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'container': string;
  'content': string;
  'editor': string;
  'fileList': string;
  'lessonName': string;
}
declare var cssExports: CssExports;
export = cssExports;

import React from 'react';
import styles from './FileList.module.scss'
import FileApi from '../../api/FileApi';

export interface FileListProps {
  user: string;
  lessonId: string;
  chapterId: string;
  file: string | undefined;
  openFile: (fileToOpen: string) => void;
}

export interface FileListState {
  files: string[];
}

export default class FileList extends React.Component<FileListProps, FileListState> {
  state = {
    files: []
  }

  componentDidMount() {
    this.fetchFileList(this.props.user, this.props.lessonId, this.props.chapterId);
  }

  componentWillReceiveProps(nextProps: FileListProps) { 
    this.fetchFileList(nextProps.user, nextProps.lessonId, nextProps.chapterId);
  }

  fetchFileList(user: string, lessonId: string, chapterId: string) {
    FileApi.getLessonFiles(lessonId, chapterId).then(files => {
      if (!this.props.file && files.length > 0) {
        this.props.openFile(files[0]);
      }
      this.setState({
        files: files
      });
    });
  }

  render() {
    return(
      <div className={styles.container}>
      {
        this.state.files.map((file, index) => 
        <div className={
          this.props.file===file ? styles.selected : ''
          } key={file} onClick={() => this.props.openFile(file)}>
          {file}
        </div>
        )
      }
      </div>

    )
  }
}
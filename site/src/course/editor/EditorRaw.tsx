import React, { useState, useEffect } from 'react';
import { useTheme, useMediaQuery, Dialog, Button, DialogTitle, DialogActions, DialogContent, Typography } from "@material-ui/core";
import FileApi from '../../api/FileApi';
import { useAuth } from '../../lib/AuthContext';

export default interface EditorRawProps {
  open: boolean;
  setOpen: (isOpen: boolean) => void;
  lessonId: string; 
  chapterId: string;
  file: string | undefined;
}

export const EditorRaw:React.FunctionComponent<EditorRawProps> = ({open, setOpen, lessonId, chapterId, file}) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
  const [code, setCode] = useState("");
  const {user} = useAuth();

  useEffect(() => {
    if (file) {
      FileApi.getLessonFile(lessonId, chapterId, file, user ? user.token : undefined)
      .then(code => {
        if (code) {
          setCode(code);
        }
      });
    }
  }, [lessonId, chapterId, file, user]);
  
  return (
    <Dialog open={open} onClose={() => setOpen(false)} aria-labelledby="form-dialog-title" maxWidth="md" fullWidth fullScreen={fullScreen}>
      <DialogTitle id="form-dialog-title">
        {file}
      </DialogTitle>
      <DialogContent>
        <Typography gutterBottom variant="body2" color="textSecondary" component={'span'}>
        <pre>{code}</pre>
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => setOpen(false)} variant="outlined" color="secondary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
}
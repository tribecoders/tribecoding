import React, { useState, useEffect, useCallback } from 'react';
import styles from './EditorContent.module.scss';
import FileApi from '../../api/FileApi';
import { useAuth } from '../../lib/AuthContext';
import { Code } from '../Code';
import Position from '../../api/Position';
import CommentApi from '../../api/CommentApi';
import Comment from '../../api/Comment';

export interface ContentProps {
  lessonId: string; 
  chapterId: string;
  file: string | undefined;
}

export const EditorContent:React.FunctionComponent<ContentProps> = ({lessonId, chapterId, file}) => {
  const [code, setCode] = useState("");
  const [comments, setComments] = useState<Comment[][]>([])
  const {user} = useAuth();
  
  const loadComments = useCallback(() => {
    CommentApi.getComments(lessonId, chapterId).then(comments => {
      const pageComments: Comment[][] = [];
      comments.forEach(comment => {
        if (comment.position.position === 'code' && comment.position.positionDetails === file) {
          const line = comment.position.line;
          if (!pageComments[line]) {
            pageComments[line] = [];
          }
          pageComments[line].push(comment);
        }
      });
      setComments(pageComments);
    })
  }, [lessonId, chapterId, file]);

  useEffect(() => {
    loadComments();
  }, [loadComments]);

  useEffect(() => {
    if (file) {
      FileApi.getLessonFile(lessonId, chapterId, file, user ? user.token : undefined)
      .then(code => {
        if (code) {
          setCode(code);
        }
      });
    }
  }, [lessonId, chapterId, file, user]);

  

  return (
    <div className={styles.editor}>
      {file && <Code code={code} position={new Position(
          lessonId,
          chapterId,
          "code",
          file
        )} comments={comments} loadComments={loadComments}></Code>
      }
    </div>
  )
}
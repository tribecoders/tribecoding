import React from 'react';
import { EditorContent } from './EditorContent';
import FileList from './FileList';
import styles from './EditorView.module.scss';
import { EditorRaw } from './EditorRaw';

export interface EditorViewProps {
  lessonId: string; 
  chapterId: string;
  open: boolean;
  setOpen: (isOpen: boolean) => void;
}

export interface EditorViewState {
  selectedFile: string | undefined;
  user: string;
}

export default class EditorView extends React.Component<EditorViewProps, EditorViewState> {
  state = {
    selectedFile: undefined,
    user: "asan",
  }

  componentWillReceiveProps(nextProps: EditorViewProps) { 
    if (nextProps.chapterId !== this.props.chapterId) {
      this.setState ({
        selectedFile: undefined
      });
    }
  }

  openFile(fileToOpen: string) {
    this.setState ({
      selectedFile: fileToOpen
    });
  }
  
  render() {
    return (
    <div className={styles.container}>
      <div className={styles.editor}>
        <div className={styles.fileList}>
          <FileList user={this.state.user} lessonId={this.props.lessonId} chapterId={this.props.chapterId} file={this.state.selectedFile} openFile={this.openFile.bind(this)}></FileList>
        </div>
        <div className={styles.content}>
          <EditorContent lessonId={this.props.lessonId} chapterId={this.props.chapterId} file={this.state.selectedFile}/>
          <EditorRaw lessonId={this.props.lessonId} chapterId={this.props.chapterId} file={this.state.selectedFile} open={this.props.open} setOpen={this.props.setOpen}/>
        </div>
      </div>

    </div>
    )
  }
}
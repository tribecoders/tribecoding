import React, { useState, useEffect, useCallback } from 'react';
import styles from './Course.module.scss';
import { Chapters } from './chapters';
import { Presentation } from './presentation';
import { Editor } from './editor';
import { Button, Grid, Container, Fab } from '@material-ui/core';
import ExitIcon from '@material-ui/icons/ExitToApp';
import DescriptionIcon from '@material-ui/icons/Description';
import CodeIcon from '@material-ui/icons/Code';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import VerticalSplitIcon from '@material-ui/icons/VerticalSplit';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import { Link, useHistory } from 'react-router-dom';
import Error from '../lib/Error';
import ReactGA from 'react-ga';
import { Lesson, Chapter, LessonApi, ChapterApi, CourseApi } from '../api';
import { useAuth } from '../lib/AuthContext';

export interface CourseProps {
  lessonSlug: string;
  chapterNo?: number;
  revision?: string;
}

export interface CourseState {
  chapters: Chapter[];
  chapterNo: number;
  displayPresentation: boolean;
  displayEditor: boolean;
  lesson: Lesson | undefined;
}

export default function Course({lessonSlug, chapterNo, revision}: CourseProps) {
  const [lesson, setLesson] = useState();
  const [chapters, setChapters] = useState<Chapter[]>([]);
  const [selectedChapter, setSelectedChapter] = useState<number>(0);
  const [displayPresentation, setDisplayPresentation] = useState(true);
  const [displayEditor, setDisplayEditor] = useState(true);
  const [displayRaw, setDisplayRaw] = useState(false);
  const [loading, setLoading] = useState(true);
  const { user } = useAuth();
  const history = useHistory();

  useEffect(() => {
    ReactGA.pageview(`/course/${lessonSlug}`);

    LessonApi.getLessonBySlug(lessonSlug).then(lesson => {
      if (lesson && lesson.id) {
        setLesson(lesson);
        ChapterApi.getLessonChapters(revision ? revision : lesson.revision).then(chapters =>{
          setChapters(chapters);
        }).finally(() => {
          setLoading(false);
        });
        if (user) {
          CourseApi.getUserChapter(user.token, lesson.id).then(chapterNo => {
            if (chapterNo) {
              setSelectedChapter(chapterNo);
            }
          })
        }
      }
    });
  },[user, lessonSlug, revision]);

  useEffect(() => {
    const resize = () => {
      if (window.innerWidth < 800) {
        showPresentation();
        ReactGA.event({
          category: 'Course',
          action: 'InitPresentation'
        });
      } else {
        ReactGA.event({
          category: 'Course',
          action: 'InitSplit'
        });
        showSplit();
      }
    }

    window.onresize = resize;
    resize();
  }, []);

  /*const publishRevision = () => {
    if (user && lesson && revision) {
      try {
        LessonApi.putLessonRevision(lesson.id, revision, user.token);
        history.push("/");
      } catch {

      }
    }
  }*/

  const selectChapter = useCallback(
    (newChapter: number) => {
        ReactGA.pageview(`/course/${lessonSlug}/chapter/${newChapter}`);
        if (history && !revision) {
          history.push(`/course/${lessonSlug}/chapter/${newChapter}`);
        }
        setSelectedChapter(parseInt(newChapter.toString()));
        window.scrollTo(0, 0);
      }

    , [history, lessonSlug, revision]
  );

  useEffect(() => {
    const select = (chapter: number) => {
      selectChapter(chapter);
    }
    if (chapterNo) {
      select(chapterNo);
    }
  }, [selectChapter, chapterNo])

  const showPresentation = () => {
    ReactGA.event({
      category: 'Course',
      action: 'Presentation'
    });
    setDisplayPresentation(true);
    setDisplayEditor(false);
  }

  const showEditor = () => {
    ReactGA.event({
      category: 'Course',
      action: 'Editor'
    });
    setDisplayEditor(true);
    setDisplayPresentation(false);
  }
  const showSplit = () => {
    ReactGA.event({
      category: 'Course',
      action: 'Split'
    });
    setDisplayEditor(true);
    setDisplayPresentation(true);
  }

  return (
    <div className={styles.container}>
      <Container maxWidth="xl">
        <Grid container spacing={3} >
          <Grid item sm={12} md={8} className={styles.chapters}>  
            <Chapters chapters={chapters.map((chapter: Chapter) => chapter.name)} chapterNo={selectedChapter} selectChapter={selectChapter}></Chapters>
          </Grid>
          <Grid item sm={12} md={4} className={styles.fullWidth}>
            <div className={styles.courseHeader}>
              <div className={styles.courseAction}>
                <Button color="secondary" onClick={showSplit} startIcon={<VerticalSplitIcon />}>
                  Split
                </Button>
                <Button color="secondary" onClick={showPresentation} startIcon={<DescriptionIcon />}>
                  Text
                </Button>
                <Button color="secondary" onClick={showEditor} startIcon={<CodeIcon />}>
                  Code
                </Button>
                {displayEditor && <Button color="secondary" onClick={() => setDisplayRaw(true)} startIcon={<FileCopyIcon />}>
                  Raw
                </Button>}
              </div>
              <div>
                {/* Autopublish enabled see server LessonController revision && lesson && lesson.revision !== revision &&
                  <Button variant="contained" color="primary" onClick={publishRevision} style={{margin: "10px"}}>Publish</Button>
                */}
                <Link to="/">
                  <Button color="secondary" startIcon={<ExitIcon />}>
                    Exit
                  </Button>
              </Link>
              </div>
            </div>
          </Grid>
        </Grid>
      </Container>
        {!loading && (
          <div>
          {lesson && chapters.length > 0 ?
            <div className={styles.courseContent}>
              {displayPresentation && 
                <Presentation lessonId={revision ? revision : lesson.revision} chapterId={(chapters[selectedChapter] as Chapter).id}>
                  <div className={styles.lessonControll}>
                    <Fab disabled={selectedChapter === 0}color="secondary" aria-label="previous" onClick={() => selectChapter(selectedChapter - 1)}>
                      <NavigateBeforeIcon />
                    </Fab>
                    <Fab disabled={selectedChapter === chapters.length - 1} color="primary" aria-label="next" onClick={() => {
                        selectChapter(selectedChapter + 1);
                        if (user) {
                          CourseApi.postUserChapter(user.token, lesson.id, selectedChapter + 1);
                        }
                      }}>
                      <NavigateNextIcon />
                    </Fab>
                  </div>
                </Presentation>
              }
              {displayEditor &&
                <Editor lessonId={revision ? revision : lesson.revision} chapterId={(chapters[selectedChapter] as Chapter).id} open={displayRaw} setOpen={setDisplayRaw}></Editor>
              }
            </div> 
            :
            <div className={styles.error}>
              <Error message="Ooops. We failed to load your course."></Error>
            </div>
          }
          </div>
        )}
    </div>
  )
}

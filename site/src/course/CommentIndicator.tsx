import React, { useState } from 'react';
import styled from 'styled-components';
import Comment from '../api/Comment';
import { CommentDialog } from './CommentDialog';
import { CommentIcon } from './presentation/CommentIcon';

export interface CommentProps {
  comments: Comment[];
}

export const CommentIndicator:React.FunctionComponent<CommentProps> = ({comments}) => {
  const [open, setOpen] = useState(false);
  const [comment, setComment] = useState();

  return (
      <span>
      { comments && 
        <Comments >
          {comments.map((comment: Comment) => {
          return (
              <CommentButton key={comment.id} onClick={() => {
                setComment(comment)
                setOpen(true);
              }}>
                <CommentIcon icon={comment.icon} />
              </CommentButton>
          )
        })}
        </Comments>   
      }
      <CommentDialog open={open} onClose={() => setOpen(false)} comment={comment}></CommentDialog>
      
    </span>
  )
}

const Comments = styled.span`
  position: absolute;
  right: -10px;
  display: block;
  color: red; //Make dependant on incon
  padding:0 10px;
`;

const CommentButton = styled.span`
  display: flex;
  flex-direction: column;
  display: block;
`
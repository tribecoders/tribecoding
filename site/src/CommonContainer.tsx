
import React from 'react';
import Header from './main/Header';
import Footer from './main/Footer';

export const CommonContainer:React.FunctionComponent = ({children}) => {
  return (
    <div>
      <Header></Header>
      {children}
      <Footer></Footer>
    </div>
  )
}
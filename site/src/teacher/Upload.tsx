import React, { useState, useEffect } from 'react';
import { Container, Button, TextField, Grid, LinearProgress } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import styles from "./Upload.module.scss";
import { Link, useHistory } from 'react-router-dom';
import Error, { ErrorContent } from "../lib/Error";
import ReactGA from 'react-ga';
import { LessonApi } from '../api';
import { useAuth } from '../lib/AuthContext';

export default function Upload() {
  const [lessonRepository, setLessonRepository] = useState("");
  const [uploadInProgress, setUploadInProgress] = useState(false);
  const [uploadSuccess, setUploadSuccess] = useState(false);
  const [error, setError] = useState<ErrorContent | undefined>(undefined);
  const history = useHistory();
  const { user } = useAuth();

  useEffect(()=> {
    ReactGA.pageview('/Upload');
  });

  const sendForm = () => {
    ReactGA.event({
      category: 'Upload',
      action: 'Upload'
    });
    if (lessonRepository) {
      setUploadInProgress(true);
      setError(undefined);
      if (user) {
        LessonApi.postLesson(lessonRepository, user.token).then(lessonRevision => {
          if (lessonRevision) {
            setLessonRepository("");
            setUploadInProgress(false);
            setUploadSuccess(true);
            ReactGA.event({
              category: 'Upload',
              action: 'Success'
            });
            setTimeout(() => {
              history.push(`/course/${lessonRevision.lessonSlug}/revision/${lessonRevision.revision}`)
            }, 1000);
          } else {
            uploadError('Your repository was not parssed correctly.', 'Parsing error!');
          }
        }).catch(() => {
          uploadError('We had a problem communicating with the server.', 'Communication error!');
        });
      }
    }
  }

  const uploadError = (message: string, title?: string) => {
    ReactGA.event({
      category: 'Upload',
      action: 'Error'
    });
    setUploadInProgress(false);
    setError(new ErrorContent(message));
  }

  const updateInputValue = (evt: any) => {
    setLessonRepository(evt.target.value);
  }

  return (
    <div>
      <Container fixed>
        <Grid container spacing={3} className={styles.header}>
          <Grid item md={6} xs={12}>
            <div className={styles.logo}>
              <Link to="/">
                <div className={styles.title}>
                  <img src="/images/logo.png" srcSet="/images/logo@2x.png 2x, /images/logo.png 1x" alt="back"/>
                  <div>TribeCoding</div>
                  </div>
              </Link>
            </div>
          </Grid>
          <Grid item md={6} xs={12} className={styles.headerRight}>
            <Link to="/user/code">your code examples</Link>
          </Grid>
        </Grid>
        {uploadSuccess ? (
          <div className={styles.thankYou}>Thank you for sharing your code!</div>
        ) : (
          <div>
            {uploadInProgress ? (
              <LinearProgress className={styles.progress}/>
            ) : (
              <div className={styles.inputContainer}>
                <TextField
                id="outlined-input"
                label="Public repository URL"
                type="text"
                name="email"
                autoComplete="email"
                margin="normal"
                variant="outlined"
                value={lessonRepository} 
                error={typeof error !== 'undefined'}
                onChange={evt => updateInputValue(evt)}
              />
                <Error error={error} className={styles.error}></Error>
                <Button 
                  onClick={sendForm} 
                  color="primary"
                  variant="contained" 
                  size="large" 
                  startIcon={<CloudUploadIcon />}
                  className ={styles.button}
                >
                      Upload git project
                </Button>
              </div>
            )}

          <div className={styles.howTo}>

            <h1>Upload guide</h1>
            <video className={styles.uploadVideo} src="/images/tribecoding.mp4" autoPlay loop muted controls></video>
            <br />Download pdf presentation <a href="/images/tribecoding.pdf" download>here</a>
            
            <h2>Demo</h2>
            See other <Link to='/'>code examples</Link> uploaded using tribecoding technology.

            <h2>How to</h2>
            Sharing your code examples never been so easy. You can do with this simple steps:

              <ul>
                <li>Check if your project already contain package.json file. If not create one.</li>
                <li>Add name, description of your code example in your package.json</li>
                <li>Create a <i>README.md file</i> describing what you did (what your code example contain).</li>
                <li>Publish your code to a git repository. You can host repository yourself or at any public vendors such as GitHub or GitLab.</li>
                <li>Provide a http link to your project git repository in the input above.</li>
                <li>Submit and we will process it.</li>
                <li>Your code is published and shared with others from programmers tribe.</li>
              </ul>

            Bellow you can find minimal required package.json structure.
            <pre>{`
{
  "name": "docker-for-web-application",
  "description": "Example usage of docker container to deploy a web application. Detailed guide how to build and run your JavaScript using Docker.",
}
              `}</pre>

            <h2>Extending package.json with tribecoding.json</h2>
            You can extend default tribecoding package.json configuration with tribecoding.json file. It has similar structure to other JSON configuration files. 
            Bellow you can find a definition: 
              <pre>{`
{
  "name": string,
  "description": string,
  "keywords": [string],
  "source": string,
  "image" : string,
  "chapters": [{
    "chapter": string,
    "branch": string,
    "file": string
  }]
}
              `}</pre>

              <ul>
                <li>name (required) - It will overide name provided in package.json file. Name will be used as your code example title.</li>
                <li>description (required) - It will overide description provided in package.json file.  Description will be used as your code example description.</li>
                <li>keywords - A list of used technologies. For example: React, Angular, Pixi, TypeScript, ...</li>
                <li>source - Relative address to your project source files directory. (default: './src')</li>
                <li>image - url to publically stored image representing your blog. We recoment 200px image height. If image is not provided the default one will be used.</li>
                <li>chapters - if you want to show code evolve in time you can create multiple chapters (branches), to represent this. By default tribecoding use master branch but you can add more.
                  <ul>
                    <li>chapter - a chapter title</li>
                    <li>branch - a branch name that contain files representing this lesson chapter. Remember to push the branch to your public git repository.</li>
                    <li>file - <i>*.md file</i> name containing chapter description. You do not need to add ".md" extension when providing a file name.</li>
                  </ul>
                </li>
              </ul>

              

              <h2>Example</h2>
                You can find example of tribeconding.json file at <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/tribecoders/petwarsclient/blob/master/tribecoding.json">here</a>. 

              <h2>Updates</h2>
              Both <i>*.json</i> and <i>*.md files</i> are fetched from master branch latest commit. This allow further updates to those files. 
              Just re-submit your repository again. Tribecoding make the latest version of your repository available with latest code examples.
            </div>
          </div>
        )}   
      </Container> 
      <Grid container spacing={3} className={styles.footer}>
        <Grid item md={6} className={styles.footerText}>
          Contact us <a href="mailto:contact@tribecoding.com">contact@tribecoding.com</a><br />
          Developed by <a href="tribecoders.com">tribecoders.com</a> 
        </Grid>
        <Grid item md={6} className={styles.footerText}>
          <a href="terms_and_conditions.html">Terms and conditions</a><br />
          <a href="cookies_policy.html">Cookies policy</a><br />
          <a href="privacy_policy.html">Privacy policy</a><br />
        </Grid>
      </Grid>
    </div>
  )
}
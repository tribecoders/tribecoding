import React, { useState, useEffect } from 'react';
import { Container, Grid } from '@material-ui/core';
import ReactGA from 'react-ga';
import { LessonApi, ChapterApi, Lesson, CourseApi } from '../api';
import { LessonCard } from '../main/lesson/LessonCard';
import { useAuth } from '../lib/AuthContext';
import styled from 'styled-components';

export default function UserCourse() {
  const [lessons, setLessons] = useState<Lesson[]>([]);
  const [enrolledLessons, setEnrolledLessons] = useState<string[]>([]);
  const {user} = useAuth();

  useEffect(() => {
    ReactGA.pageview('/UserCourse');
  },[]);

  useEffect(() => {
    LessonApi.getLessons().then(lessons => {
      setLessons(lessons);

      lessons.forEach(lesson => {
        ChapterApi.getLessonChapters(lesson.id).then(chapters => {
          lesson.chapters = chapters;
          setLessons(lessons);
        })
      });
    });
  },[]);

  useEffect(() => {
    if (user) {
      CourseApi.getEnrolledLessons(user.token).then(enrolledLessons => {
        if (enrolledLessons && enrolledLessons.length > 0) {
          setEnrolledLessons(enrolledLessons);
        }
      })
    }
  },[user]);

  return (
      <Container fixed style={containerStyle}>
        <Title>My code examples</Title>
        <Grid container spacing={3}>
        { lessons.map((lesson: Lesson) =>
        <Grid key={lesson.id} item xs={12} md={4}>
          {enrolledLessons.includes(lesson.id) &&
            <LessonCard lesson={lesson} isEnrolled={true}/>
          }
        </Grid>
        )}
        </Grid>
      </Container>
  )
}

const Title = styled.h1 `
  text-align: center;
`

const containerStyle = {
  marginTop: '20px'
}
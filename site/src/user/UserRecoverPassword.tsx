import React, { useState } from 'react';
import { Container, Grid, TextField, Button } from '@material-ui/core';
import { UserApi, handleErrors } from '../api';
import { useStatus } from '../lib/StatusContext';
import { useHistory } from 'react-router-dom';

export interface UserRecoverPasswordProps {
  match:{
    params: {
      token: string;
    }
  }
}

export const UserRecoverPassword:React.FunctionComponent<UserRecoverPasswordProps> = ({match}) => {
  const [errors, setErrors] = useState<Map<string, string>>(new Map());
  const { setMessage } = useStatus();
  const history = useHistory();
  
  const handleSubmit = (event: any) => {
    event.preventDefault();
    const password = event.target.password.value;
    if (match.params.token) {
      const [user, time, hash] = match.params.token.split('-')

      UserApi.recoverPassword(time, user, hash, password).then(success => {
        if (success) {
          setMessage('Password changed.', 'success');
          history.push('/');
        }
      }).catch((err) => {
        console.error(err);
        handleErrors(err.errors, setMessage, setErrors);
      });
    } else {
      setMessage('Security token missing.', 'error');
    }
  
  }

  const clearParamError = (event: any) => {
    const param = event.target.name;
    var newErrors = new Map(errors)
    newErrors.delete(param);
    setErrors(newErrors);
  }

  return (
      <Container fixed>
        <Grid container spacing={3} justify="center">
          <Grid item md={6} sm={12}>
            <h1>Please provide new password</h1>
            <form onSubmit={handleSubmit} noValidate>
              <TextField
                    margin="dense"
                    name="password"
                    label="Password"
                    type="password"
                    error={errors.has("password")}
                    helperText={errors.get("password")}
                    autoComplete="password"
                    fullWidth
                    onBlur={clearParamError}
                  />
              <p>
                <Button type="submit" variant="outlined" color="primary">
                  Update
                </Button>
              </p>
            </form>
          </Grid>
        </Grid>
      </Container>
  )
}
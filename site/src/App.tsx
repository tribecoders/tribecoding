import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import { MainRoute } from './MainRoute';
import { CourseRoute } from './CourseRoute';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core';
import { green, grey } from '@material-ui/core/colors';
import CookieConsent from 'react-cookie-consent';
import { AuthContext } from './lib/AuthContext';
import User from './api/User';
import { StatusContext } from './lib/StatusContext';
import { StatusVariant } from './lib/StatusMessage';
import { Status } from './lib/Status';
import { TeacherRoute } from './TeacherRoute';
import UserCourse from './user/UserCourse';
import { UserRecoverPassword } from './user/UserRecoverPassword';
import { CommonContainer } from './CommonContainer';

export default function App() {
  const authStore = `tribecoding_auth_${process.env.NODE_ENV}`;

  const theme = createMuiTheme({
    palette: {
      primary: green,
      secondary: grey
    }
  });

  const [user, setUser] = useState();
  const [statusMessage, setStatusMessage] = useState<string>();
  const [statusVariant, setStatusVariant] = useState<keyof typeof StatusVariant>();
  const [statusOpen, setStatusOpen] = useState(false);

  const setMessage = (message: string, variant: keyof typeof StatusVariant) => {
    setStatusMessage(message);
    setStatusVariant(variant);
    setStatusOpen(true);
  }

  const handleClose = () => {
    setStatusOpen(false);
  };
  
  const setAuthUser = (user: User | undefined) => {
    if (user) {
      localStorage.setItem(authStore, JSON.stringify(user));
    } else {
      localStorage.removeItem(authStore);
    }
    setUser(user);
  }

  useEffect(() => {
    const userData = localStorage.getItem(authStore);
    if (userData) {
      // Might be usefull if token was not blacklisted
      const user = JSON.parse(userData);
      if (user) {
        setUser(user);
      }
    }
  }, [authStore]);

  return (
    <React.Fragment>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <StatusContext.Provider value={{setMessage}}>
        <AuthContext.Provider value={{user, setUser: setAuthUser}}>
        <BrowserRouter>
          <Switch>
            <Route path="/course/:lessonSlug/chapter/:chapterNo" component={CourseRoute} />
            <Route path="/course/:lessonSlug/revision/:revision" component={CourseRoute} />
            <Route path="/course/:lessonSlug" component={CourseRoute} />
            <Route path="/user">
              <CommonContainer>
                <Route path="/user/code" component={UserCourse} /> 
                <Route path="/user/recover/:token" component={UserRecoverPassword} />
              </CommonContainer>
            </Route> 
            <Route path="/publish" component={TeacherRoute} />
            <Route path="/" component={MainRoute}/>
          </Switch>
        </BrowserRouter>
        </AuthContext.Provider>
        <CookieConsent style={{ padding: "15px" }}>
          This website uses cookies to enhance the user experience. For more details see our <a href="cookies_policy.html">Cookies Policy</a>
        </CookieConsent>
          {statusMessage && statusVariant &&
            <Status message={statusMessage} variant={statusVariant} open={statusOpen} onClose={handleClose} />
          }
        </StatusContext.Provider>
      </ThemeProvider>
    </React.Fragment>
  )
}
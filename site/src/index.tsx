import React from 'react';
import ReactDOM from 'react-dom';
import ReactGA from 'react-ga';
import App from './App';

ReactGA.initialize('UA-153377497-1');
const appElement = document.getElementById('app');
ReactDOM.render((
  <App></App>
)
, appElement);

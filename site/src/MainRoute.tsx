import React from 'react';
import { Main } from './main';
import { CommonContainer } from './CommonContainer';

export class MainRoute extends React.Component {
  render() {
    return (
      <CommonContainer>
        <Main />
      </CommonContainer>
    )
  }
}
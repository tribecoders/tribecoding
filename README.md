# Tribecoding app

A learning platform witch easy code chapters based on git branches.

## Structure

Tribecoding app consict of 4 elements

- Site
- Server
- File server
- Database

## Deploy

1. Create enviroment variables in _.env_ file

```
GIT_LOGIN=tribecoding
GIT_PASS=********
POSTGRES_USER=tribecoding
POSTGRES_PASSWORD=********
POSTGRES_DB=tribecoding
TOKEN_SECRET=**********
```

2. Get latest docker images, _docker-compose.yml_ configuration, and run

```
./deploy.sh
```

3. SSL

- get certificate files
- name files tribecoding.crt, tribecoding.key
- on the server mkdir /etc/nginx/ssl and move files there

4. Expose application to through nginx reverse proxy by seting to nginx_sites_available.json (this will also set ssl):

```
/etc/nginx/sites-available/default
```
